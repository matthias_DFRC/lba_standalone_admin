const proxy = require("http-proxy-middleware");

module.exports = function(app) {
  app.use(
    proxy("/LBALora/GUI", {
      // target: "http://192.168.0.10/",
      target: "http://116.38.162.38:12300/",
      changeOrigin: true
    })
  );
};
// home/ubuntu/www/lbaAdmin/buildW

import React from "react";
import PropTypes from "prop-types";

import { Message } from "semantic-ui-react";

const FormErrorMessage = ({ text, subText, customClass }) => {
  return (
    <Message negative className={customClass} style={{ margin: "1em" }}>
      <Message.Header>{text}</Message.Header>
      <p>{subText}</p>
    </Message>
  );
};

FormErrorMessage.propTypes = {
  text: PropTypes.string.isRequired
};

export default FormErrorMessage;

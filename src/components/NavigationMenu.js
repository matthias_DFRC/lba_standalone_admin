import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { actions as globalActions } from "../redux/actions/global.action";
import { getAppStatus } from "../redux/reducers/global.reducer";
import { userAccount } from "../utils/userAccountUtils";
import { Link } from "react-router-dom";
import { Menu, Button, Header, Icon, Popup, Dropdown } from "semantic-ui-react";

import { STRINGS } from "../constants/strings";
import { ROUTES, STATUS, LANGUAGE_OPTIONS, VERSION } from "../constants";
import i18n from "../i18n";
import { sessionstorage } from "../utils/sessionstorage";

const style = {
  activeMenu: {
    backgroundColor: "#fff",
    padding: "0.5em 1em 0.5em 1em",
    borderRadius: "1em",
    color: "rgba(0,0,0,0.85)",
    activeItem: "status"
  }
};

class NavigationMenu extends Component {
  state = {
    activeItem: STRINGS.menu["status"]
  };
  constructor(props) {
    super(props);

    const { loadDataFromSessionStorage } = props;

    if (userAccount.isAuthenticated()) {
      loadDataFromSessionStorage();
    }
  }

  static getDerivedStateFromProps(props) {
    if (props.appStatus !== undefined) {
      return {};
    }
    return null;
  }

  componentDidMount() {
    this.mount = true;
    this.props.i18n.changeLanguage(sessionStorage.getItem("language"));
  }

  componentDidUpdate = async (prevProps, prevState) => {
    if (prevProps.appStatus !== this.props.appStatus) {
      await this.setState(prevState => ({
        ...prevState,
        activeItem: "status"
      }));
      if (this.props.appStatus === STATUS.INITIALIZED) {
      }
    }
  };

  componentWillUnmount() {
    this.mount = false;
  }

  handleItemClick = async name => {
    await this.setState({ activeItem: name });
  };

  handleLanguageChange = async () => {
    let language = sessionStorage.getItem("language");
    language === "kr" ? (language = "en") : (language = "kr");

    await this.setState({ language: language });
    await sessionStorage.setItem("language", this.state.language);
    await this.props.i18n.changeLanguage(sessionStorage.getItem("language"));
    window.location.reload();
  };

  render() {
    return (
      <div>
        <Menu
          vertical
          fixed="left"
          size="large"
          style={{
            marginTop: "4em",
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          <div>
            {this._renderMenuItem("status")}
            {this._renderMenuItem("sensorsDetails")}
            {this._renderMenuItem("ipConfig")}
            {this._renderMenuItem("restart")}
            {this._renderMenuItem("reset")}
            {/* {this._renderMenuItem("softwareUpdate")} */}
          </div>
          <div>
            <Menu.Item
              style={{
                textAlign: "center"
              }}
            >
              Version {VERSION}
            </Menu.Item>
          </div>
        </Menu>

        <Menu
          fluid
          widths={3}
          fixed="top"
          borderless
          size="massive"
          style={{ display: "flex" }}
        >
          <Menu.Item />
          <Menu.Item style={{ color: "#666" }}>Configuration Panel</Menu.Item>

          <Menu.Item style={{ justifyContent: "flex-end" }}>
            <span
              style={{ cursor: "pointer", marginRight: "1em" }}
              onClick={this.handleLanguageChange}
            >
              {i18n.language === "kr" ? "한국어" : "English"}{" "}
              <Icon name="language" />
            </span>
            <Popup
              trigger={
                <Button
                  basic
                  circular
                  size="mini"
                  style={{ marginRight: "3em" }}
                >
                  <Icon name="user" />
                </Button>
              }
              content={
                <span
                  onClick={() => {
                    sessionStorage.clear();
                    this.props.history.push("../");
                  }}
                  style={{ cursor: "pointer" }}
                >
                  <Icon name="power off" />
                  Log-Out
                </span>
              }
              flowing
              position="bottom center"
              on="click"
            />
          </Menu.Item>
        </Menu>
      </div>
    );
  }

  _renderMenuItem = path => {
    const { activeItem } = this.state;
    // console.log(this.props.history.location.pathname);
    return (
      <Link to={ROUTES[path]}>
        <Menu.Item
          name={path}
          active={this.props.history.location.pathname.includes(path)}
          onClick={(e, { name }) => this.handleItemClick(name)}
        >
          <span
            style={{
              fontWeight: activeItem === path ? "bold" : "",
              color: "#666"
            }}
          >
            {path === "status" ? <Icon name="info circle" /> : ""}
            {path === "sensorsDetails" ? <Icon name="zoom" /> : ""}
            {path === "ipConfig" ? <Icon name="settings" /> : ""}
            {path === "reset" ? <Icon name="repeat" /> : ""}
            {path === "restart" ? <Icon name="power cord" /> : ""}
            {STRINGS.menu[path]()}
          </span>
        </Menu.Item>
      </Link>
    );
  };
}

const mapStateToProps = state => {
  return {
    appStatus: getAppStatus(state)
  };
};

const mapDispatchToProps = () => {
  return {
    loadDataFromSessionStorage: globalActions.loadDataFromSessionStorage
  };
};

export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps())(NavigationMenu)
);

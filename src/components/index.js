import FormErrorMessage from './FormErrorMessage';
import NavigationMenu from './NavigationMenu';

export const COMPONENTS = {
  FormErrorMessage,
  NavigationMenu
};

import React, { Component } from "react";
import {
  Container,
  Header,
  Segment,
  Grid,
  Dimmer,
  Loader,
  Divider,
  Button
} from "semantic-ui-react";
import { connect } from "react-redux";
import { formatRequestParams } from "../../utils/requestFormatter";
import "../../styles/body.scss";
import { actions } from "../../redux/actions/sensorsDetails.action";
import {
  getAccountCredentials,
  getSensorStatusList
} from "../../redux/reducers/global.reducer";
import { getSensorStatusListStatus } from "../../redux/reducers/sensorsDetails.reducer";
import { STATUS } from "../../constants";

class SensorsDetails extends Component {
  state = {
    error: false,
    errorMessage: undefined,
    sensorStatusList: undefined,
    sensorStatusListStatus: undefined
  };
  constructor(props) {
    super(props);
  }

  static getDerivedStateFromProps(props, state) {
    const { sensorStatusList, sensorStatusListStatus } = props;

    if (sensorStatusListStatus !== state.sensorStatusListStatus) {
      return {
        ...state,
        sensorStatusList,
        sensorStatusListStatus
      };
    }

    return null;
  }

  componentDidMount = () => {
    this.mounted = true;
    this.fetchStatusApis();
    // console.log(this.state);
  };

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const { sensorStatusListStatus } = this.props;
    if (sensorStatusListStatus !== prevProps.sensorStatusListStatus) {
    }
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  fetchStatusApis = async () => {
    const { user, pass } = this.props.credential;

    try {
      this.getSensorStatusList(user, pass);
    } catch (e) {
      await this.setState({
        errorMessage: e
      });
    }
  };

  getSensorStatusList = async (user, pass) => {
    const { sensorStatusListApiRequest } = this.props;

    const payload = formatRequestParams.simpleRequest({ user, pass });

    await sensorStatusListApiRequest(payload);
  };

  _renderSensors() {
    const { sensorStatusList } = this.state;
    if (sensorStatusList) {
      return sensorStatusList.map(sensor => {
        return (
          <Grid.Row
            key={sensor.sensorId}
            style={{
              marginTop: "5em",
              justifyContent: "center"
            }}
          >
            <Segment
              inverted
              textAlign="center"
              color={sensor.sensorStatus === "online" ? "green" : "red"}
            >
              Sensor Id: {sensor.sensorId} - {sensor.sensorStatus}
              <Divider hidden />
              {this._renderSapCount(sensor)}
            </Segment>
          </Grid.Row>
        );
      });
    }
  }

  _renderSapCount = sensor => {
    if (
      sensor.sensorStatus === "online" &&
      sensor.associateSiteDTOList.length > 0
    ) {
      return (
        <div>
          {sensor.associateSiteDTOList[0].sapRegionResponse.length > 0
            ? `Sap Count: ${sensor.associateSiteDTOList[0].sapRegionResponse[1].uniqueCount}`
            : "empty"}
        </div>
      );
    } else {
      return <div>Sap Count: null</div>;
    }
  };

  _renderLoader = () => {
    return (
      <Dimmer active inverted>
        <Loader inverted>Loading</Loader>
      </Dimmer>
    );
  };

  _renderErrorPage = () => {
    return (
      <Grid.Row
        style={{
          marginTop: "5em",
          justifyContent: "center"
        }}
      >
        <Segment textAlign="center">
          Sorry Error
          <br />
          <Button
            onClick={() => {
              this.props.history.push("./status");
            }}
          >
            Go Back
          </Button>
        </Segment>
      </Grid.Row>
    );
  };

  render() {
    const { sensorStatusListStatus } = this.state;
    if (sensorStatusListStatus === STATUS.OK) {
      return (
        <Container className="bodyContainer" fluid>
          {/* <Header>Sensors' Status</Header> */}
          <Grid style={{ display: "flex" }}>{this._renderSensors()}</Grid>
        </Container>
      );
    } else if (sensorStatusListStatus === STATUS.LOADING) {
      return this._renderLoader();
    } else {
      return (
        <Container className="bodyContainer" fluid>
          <Grid style={{ marginTop: "5em", display: "flex" }}>
            {this._renderErrorPage()}
          </Grid>
        </Container>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    credential: getAccountCredentials(state),
    sensorStatusList: getSensorStatusList(state),
    sensorStatusListStatus: getSensorStatusListStatus(state)
  };
};

const mapDispatchToProps = () => {
  return {
    sensorStatusListApiRequest: actions.sensorStatusListApiRequest
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(SensorsDetails);

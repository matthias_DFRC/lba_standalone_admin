import React, { Component } from "react";
import {
  Container,
  Segment,
  Grid,
  Card,
  Loader,
  Dimmer,
  Icon
} from "semantic-ui-react";
import { connect } from "react-redux";
import { formatRequestParams } from "../../utils/requestFormatter";
import "../../styles/body.scss";
import { actions } from "../../redux/actions/status.action";
import {
  getAccountCredentials,
  getSensorStatus,
  getSapCount,
  getFreeDiskSize,
  getLocalIpAddress,
  getCloudConnection,
  getCurrentNetworkConfig
} from "../../redux/reducers/global.reducer";
import {
  getSensorStatusStatus,
  getSapCountStatus,
  getFreeDiskSizeStatus,
  getLocalIpAddressStatus,
  getCloudConnectionStatus,
  getCurrentNetworkConfigStatus
} from "../../redux/reducers/status.reducer";
import { VictoryPie, VictoryContainer } from "victory";
import { ROUTES } from "../../constants";
import { STRINGS } from "../../constants/strings";

class Status extends Component {
  state = {
    error: false,
    errorMessage: undefined,
    sensorStatus: undefined,
    sapCount: undefined,
    freeDiskSize: {
      availableDiskSize: undefined,
      diskSize: undefined
    },
    currentNetworkConfig: [],
    isLoaded: false
  };
  constructor(props) {
    super(props);
  }

  static getDerivedStateFromProps(props, state) {
    const {
      sensorStatus,
      sensorStatusStatus,
      sapCount,
      sapCountStatus,
      freeDiskSize,
      freeDiskSizeStatus,
      localIpAddress,
      localIpAddressStatus,
      currentNetworkConfig,
      currentNetworkConfigStatus
    } = props;

    if (sensorStatusStatus !== state.sensorStatusStatus) {
      return {
        ...state,
        sensorStatus,
        sensorStatusStatus
      };
    }

    if (sapCountStatus !== state.sapCountStatus) {
      return {
        ...state,
        sapCount,
        sapCountStatus
      };
    }

    if (freeDiskSizeStatus !== state.freeDiskSizeStatus) {
      return {
        ...state,
        freeDiskSize,
        freeDiskSizeStatus
      };
    }

    if (localIpAddressStatus !== state.localIpAddressStatus) {
      return {
        ...state,
        localIpAddress,
        localIpAddressStatus
      };
    }

    if (currentNetworkConfigStatus !== state.currentNetworkConfigStatus) {
      return {
        ...state,
        currentNetworkConfig,
        currentNetworkConfigStatus
      };
    }

    return null;
  }

  componentDidMount = async () => {
    await this.fetchStatusApis();
    await this.setState({ isLoaded: true });
  };

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const { sensorStatusStatus } = this.props;
    if (sensorStatusStatus !== prevProps.sensorStatusStatus) {
    }
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  fetchStatusApis = () => {
    const { user, pass, sensorId } = this.props.credential;

    try {
      this.getSensorStatus(user, pass, sensorId);
      this.getSapCount(user, pass);
      this.getFreeDiskSize(user, pass);
      this.getLocalIpAddress(user, pass);
      this.getCloudConnection(user, pass);
      this.getCurrentNetworkConfig(user, pass);
    } catch (e) {
      console.log(e);
      this.setState({
        errorMessage: e
      });
    }
  };

  getSensorStatus = async (user, pass, sensorId) => {
    const { sensorStatusApiRequest } = this.props;

    const payload = {
      user,
      pass,
      sensorId
    };

    await sensorStatusApiRequest(payload);
  };

  getSapCount = async (user, pass) => {
    const { sapCountApiRequest } = this.props;

    const payload = formatRequestParams.simpleRequest({ user, pass });

    await sapCountApiRequest(payload);
  };

  getFreeDiskSize = async (user, pass) => {
    const { freeDiskSizeApiRequest } = this.props;

    const payload = formatRequestParams.simpleRequest({ user, pass });

    await freeDiskSizeApiRequest(payload);
  };

  getLocalIpAddress = async (user, pass) => {
    const { localIpAddressApiRequest } = this.props;

    const payload = formatRequestParams.simpleRequest({ user, pass });

    await localIpAddressApiRequest(payload);
  };

  getCloudConnection = async (user, pass) => {
    const { cloudConnectionApiRequest } = this.props;

    const payload = formatRequestParams.simpleRequest({ user, pass });

    await cloudConnectionApiRequest(payload);
  };

  getCurrentNetworkConfig = async (user, pass) => {
    const { currentNetworkConfigApiRequest } = this.props;

    const payload = formatRequestParams.simpleRequest({ user, pass });

    await currentNetworkConfigApiRequest(payload);
  };

  _renderDiskStatus() {
    const { freeDiskSize } = this.props;
    const availableSpace =
      (freeDiskSize.availableDiskSize / freeDiskSize.diskSize) * 100;
    if (availableSpace > 90) {
      return STRINGS.status["veryGood"]();
    } else if (availableSpace > 40) {
      return STRINGS.status["good"]();
    } else if (availableSpace > 20) {
      return STRINGS.status["low"]();
    } else {
      return STRINGS.status["criticallyLow"]();
    }
  }
  _renderLoader = () => {
    return (
      <Dimmer active inverted>
        <Loader inverted>Loading</Loader>
      </Dimmer>
    );
  };

  render() {
    const {
      sensorStatus,
      freeDiskSize,
      cloudConnection,
      isLoaded
    } = this.props;

    const styles = {
      cardStyle: { justifyContent: "center", display: "block" },
      cardTextStyle: { textAlign: "center", height: "13em" },
      pStyle: {
        position: "absolute",
        left: "55%",
        marginLeft: "-55px",
        top: "60%",
        marginTop: "-40px"
      },
      headerColor: {
        color: "#666666"
      }
    };

    if (!isLoaded) {
      return (
        <Container className="bodyContainer" fluid>
          <Grid style={{ display: "flex", marginLeft: "5em" }}>
            <Grid.Row columns={2}>
              <Grid.Column style={{ justifyContent: "center" }}>
                <Card
                  fluid
                  style={styles.cardStyle}
                  onClick={() => {
                    this.props.history.push("./sensorsDetails");
                  }}
                >
                  <Card.Content style={styles.headerColor}>
                    <h4>{STRINGS.status["sensorHealth"]()}</h4>
                  </Card.Content>
                  <Card.Content style={styles.cardTextStyle}>
                    <p
                      style={{
                        color: sensorStatus === "online" ? "green" : "red",
                        position: "absolute",
                        left: "55%",
                        marginLeft: "-55px",
                        top: "60%",
                        marginTop: "-40px"
                      }}
                    >
                      {sensorStatus}
                      &nbsp;
                      <Icon
                        name="circle"
                        color={sensorStatus === "online" ? "green" : "red"}
                        size="large"
                      />
                    </p>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid style={styles.cardStyle}>
                  <Card.Content style={styles.headerColor}>
                    <h4>{STRINGS.status["diskSize"]()}</h4>
                  </Card.Content>
                  <Card.Content
                    style={{ textAlign: "center", display: "flex  " }}
                  >
                    <p style={{ marginTop: "5em", marginLeft: "3em" }}>
                      {STRINGS.status["diskSizeStatus"]()}:{" "}
                      {this._renderDiskStatus()}
                    </p>
                    <VictoryPie
                      colorScale={["green", "tomato"]}
                      height={150}
                      innerRadius={10}
                      data={[
                        {
                          x: STRINGS.status["available"](),
                          y: freeDiskSize.availableDiskSize
                        },
                        {
                          x: STRINGS.status["used"](),
                          y:
                            freeDiskSize.diskSize -
                            freeDiskSize.availableDiskSize
                        }
                      ]}
                      containerComponent={
                        <VictoryContainer
                          responsive={false}
                          style={{ margin: "auto" }}
                        />
                      }
                      labels={({ datum }) =>
                        `${((datum.y / freeDiskSize.diskSize) * 100).toFixed(
                          2
                        )}%`
                      }
                      events={[
                        {
                          target: "data",
                          eventHandlers: {
                            onClick: datum => {
                              return [
                                {
                                  target: "data",
                                  mutation: ({ style }) => {
                                    return style.fill === "#c43a31"
                                      ? null
                                      : { style: { fill: "#c43a31" } };
                                  }
                                },
                                {
                                  target: "labels",
                                  mutation: ({ text, datum }) => {
                                    return text ===
                                      `${datum.x} ` + STRINGS.status["size"]()
                                      ? null
                                      : {
                                          text:
                                            `${datum.x} ` +
                                            STRINGS.status["size"]()
                                        };
                                  }
                                }
                              ];
                            }
                          }
                        }
                      ]}
                    />
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2} style={{ marginTop: "2em" }}>
              <Grid.Column>
                <Card fluid style={styles.cardStyle}>
                  <Card.Content style={styles.headerColor}>
                    <h4>{STRINGS.status["cloudConnection"]()}</h4>
                  </Card.Content>
                  <Card.Content style={styles.cardTextStyle}>
                    <p style={styles.pStyle}>
                      {cloudConnection ? "Yes" : "No"}
                    </p>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card
                  fluid
                  style={styles.cardStyle}
                  onClick={() => {
                    this.props.history.push("./ipConfig");
                  }}
                >
                  <Card.Content style={styles.headerColor}>
                    <h4>{STRINGS.status["networkType"]()}</h4>
                  </Card.Content>
                  <Card.Content style={styles.cardTextStyle}>
                    <span style={styles.pStyle}>
                      {this._renderNetworkType()}
                    </span>
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2} style={{ marginTop: "2em" }}>
              <Grid.Column>
                <Card fluid style={styles.cardStyle}>
                  <Card.Content style={styles.headerColor}>
                    <h4>Comport</h4>
                  </Card.Content>
                  <Card.Content style={styles.cardTextStyle}>
                    <p style={styles.pStyle}>{sensorStatus}</p>
                  </Card.Content>
                </Card>
              </Grid.Column>
              <Grid.Column>
                <Card fluid style={styles.cardStyle}>
                  <Card.Content style={styles.headerColor}>
                    <h4>{STRINGS.status["localIp"]()}</h4>
                  </Card.Content>
                  <Card.Content style={styles.cardTextStyle}>
                    <span style={styles.pStyle}>{this._renderLocalIp()}</span>
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      );
    } else {
      return this._renderLoader();
    }
  }

  _renderNetworkType = () => {
    const { currentNetworkConfig } = this.state;
    if (currentNetworkConfig.length > 0) {
      return currentNetworkConfig.map(config => {
        return (
          <p key={config.network} style={{ color: "black" }}>
            {config.network}
          </p>
        );
      });
    } else {
      return "";
    }
  };

  _renderLocalIp = () => {
    const { currentNetworkConfig } = this.state;
    if (currentNetworkConfig.length > 0) {
      return currentNetworkConfig.map(config => {
        return <p key={config.ipAddress}>{config.ipAddress}</p>;
      });
    } else {
      return "";
    }
  };
}

const mapStateToProps = state => {
  return {
    credential: getAccountCredentials(state),
    sensorStatus: getSensorStatus(state),
    sensorStatusStatus: getSensorStatusStatus(state),
    sapCount: getSapCount(state),
    sapCountStatus: getSapCountStatus(state),
    freeDiskSize: getFreeDiskSize(state),
    freeDiskSizeStatus: getFreeDiskSizeStatus(state),
    localIpAddress: getLocalIpAddress(state),
    localIpAddressStatus: getLocalIpAddressStatus(state),
    cloudConnection: getCloudConnection(state),
    cloudConnectionStatus: getCloudConnectionStatus(state),
    currentNetworkConfig: getCurrentNetworkConfig(state),
    currentNetworkConfigStatus: getCurrentNetworkConfigStatus(state)
  };
};

const mapDispatchToProps = () => {
  return {
    sensorStatusApiRequest: actions.sensorStatusApiRequest,
    sapCountApiRequest: actions.sapCountApiRequest,
    freeDiskSizeApiRequest: actions.freeDiskSizeApiRequest,
    localIpAddressApiRequest: actions.localIpAddressApiRequest,
    cloudConnectionApiRequest: actions.cloudConnectionApiRequest,
    currentNetworkConfigApiRequest: actions.currentNetworkConfigApiRequest
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Status);

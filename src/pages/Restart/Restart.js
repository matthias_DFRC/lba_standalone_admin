import React, { Component } from "react";
import { Container, Header, Button, Segment, Divider } from "semantic-ui-react";
import { connect } from "react-redux";
import { formatRequestParams } from "../../utils/requestFormatter";
import "../../styles/body.scss";
import { actions } from "../../redux/actions/restart.action";
import { getRestartStatus } from "../../redux/reducers/restart.reducer";
import {
  getRestart,
  getAccountCredentials
} from "../../redux/reducers/global.reducer";
import { STRINGS } from "../../constants/strings";

class Restart extends Component {
  state = {
    error: false
  };

  static getDerivedStateFromProps(props, state) {
    const { restart, restartStatus } = props;

    if (restartStatus !== state.restartStatus) {
      return {
        ...state,
        restart,
        restartStatus
      };
    }
    return null;
  }

  componentDidMount = () => {
    this.mounted = true;
  };

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const {} = this.props;
    // if (resolution !== prevProps.resolution) {
    //   this.fetchVisitorVehiclesApis(siteId, currentDate, previousDate, resolution);
    // }
    // console.log(this.props);
    // console.log(this.state);
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  postRestart = async (user, pass) => {
    const { restartApiRequest } = this.props;

    // const payload = formatRequestParams.simpleRequest({ user, pass });
    const payload = {
      user,
      pass
    };

    // console.log("calling restart api");
    await restartApiRequest(payload);
    window.alert("The system is going to restart. Please login again");
    sessionStorage.clear();
    this.props.history.push("../");
  };

  render() {
    const { user, pass } = this.props.credential;

    return (
      <Container className="bodyContainer" fluid>
        <Segment textAlign="center" style={{ marginLeft: "5em" }}>
          <Header as="h2">
            {STRINGS.restart["restartTitle"]()}
            <Header.Subheader>
              {STRINGS.restart["restartBody"]()}
            </Header.Subheader>
          </Header>
          <Divider section />
          <Button.Group>
            <Button
              onClick={() => {
                this.props.history.push("./status");
              }}
            >
              {" "}
              No{" "}
            </Button>
            <Button.Or />
            <Button positive onClick={() => this.postRestart(user, pass)}>
              Yes
            </Button>
          </Button.Group>
        </Segment>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    credential: getAccountCredentials(state),
    restart: getRestart(state),
    restartStatus: getRestartStatus(state)
  };
};

const mapDispatchToProps = () => {
  return {
    restartApiRequest: actions.restartApiRequest
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Restart);

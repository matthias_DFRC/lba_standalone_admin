import React, { Component } from "react";
import {
  Card,
  Container,
  Grid,
  Dimmer,
  Header,
  Segment,
  Form,
  Checkbox,
  Dropdown,
  Button,
  Input,
  Divider,
  Loader
} from "semantic-ui-react";
import { connect } from "react-redux";
import { formatRequestParams } from "../../utils/requestFormatter";
import "../../styles/body.scss";
import { actions } from "../../redux/actions/ipConfig.action";
import {
  getAccountCredentials,
  getConfigureDynamicIp,
  getConfigureStaticIp,
  getConfigureDynamicWifiIp,
  getConfigureStaticWifiIp,
  getConfigureLteModem,
  getCurrentNetworkConfigList
} from "../../redux/reducers/global.reducer";
import {
  getConfigureDynamicIpStatus,
  getConfigureStaticIpStatus,
  getConfigureStaticWifiIpStatus,
  getConfigureDynamicWifiIpStatus,
  getConfigureLteModemStatus,
  getCurrentNetworkConfigListStatus
} from "../../redux/reducers/ipConfig.reducer";
import { STRINGS } from "../../constants/strings";

class IpConfig extends Component {
  state = {
    dynamicIpAddress: true,
    dynamicWifiIpAddress: true,
    letModem: true,
    subnetMasks: [
      { key: 1, text: "255.255.255.0", value: 24 },
      { key: 2, text: "255.255.255.128", value: 25 },
      { key: 3, text: "255.255.255.192", value: 26 },
      { key: 4, text: "255.255.255.224", value: 27 },
      { key: 5, text: "255.255.255.240", value: 28 },
      { key: 6, text: "255.255.255.248", value: 29 },
      { key: 7, text: "255.255.255.252", value: 30 }
    ],
    subnetMask: undefined,
    staticIp: " ",
    staticGateWay: " ",
    staticPreferredDNS: " ",
    staticAlternateDNS: " ",
    wifiSubnetMask: undefined,
    staticWifiIp: " ",
    staticWifiGateWay: " ",
    staticWifiPreferredDNS: " ",
    staticWifiAlternateDNS: " ",
    wifiSSID: " ",
    wifiPass: " ",
    apn: undefined,
    configureDynamicIpStatus: undefined,
    configureDynamicWifiIpStatus: undefined,
    configureStaticIpStatus: undefined,
    configureStaticWifiIpStatus: undefined,
    configureLteModemStatus: undefined,
    networkType: "",
    currentNetworkConfigList: [],
    currentNetworkConfigListStatus: undefined,
    error: false,
    styles: {
      formField: { marginTop: "1em" },
      card: { width: "100%", marginTop: "2em" }
    }
  };

  constructor(props) {
    super(props);
  }

  static getDerivedStateFromProps(props, state) {
    const {
      configureDynamicIp,
      configureDynamicIpStatus,
      configureStaticIpStatus,
      configureStaticIp,
      configureStaticWifiIpStatus,
      configureStaticWifiIp,
      configureDynamicWifiIpStatus,
      configureDynamicWifiIp,
      configureLteModemStatus,
      configureLteModem,
      currentNetworkConfigList,
      currentNetworkConfigListStatus
    } = props;

    if (configureDynamicIpStatus !== state.configureDynamicIpStatus) {
      return {
        ...state,
        configureDynamicIp,
        configureDynamicIpStatus
      };
    }

    if (configureStaticIpStatus !== state.configureStaticIpStatus) {
      return {
        ...state,
        configureStaticIp,
        configureStaticIpStatus
      };
    }

    if (configureStaticWifiIpStatus !== state.configureStaticWifiIpStatus) {
      return {
        ...state,
        configureStaticWifiIp,
        configureStaticWifiIpStatus
      };
    }

    if (configureDynamicWifiIpStatus !== state.configureDynamicWifiIpStatus) {
      return {
        ...state,
        configureDynamicWifiIp,
        configureDynamicWifiIpStatus
      };
    }

    if (configureLteModemStatus !== state.configureLteModemStatus) {
      return {
        ...state,
        configureLteModem,
        configureLteModemStatus
      };
    }

    if (
      currentNetworkConfigListStatus !== state.currentNetworkConfigListStatus
    ) {
      return {
        ...state,
        currentNetworkConfigList,
        currentNetworkConfigListStatus
      };
    }
    return null;
  }

  componentDidMount = async () => {
    await this.fetchIpConfigApis();
    this.mounted = true;
  };

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const {
      configureDynamicIpStatus,
      configureStaticIpStatus,
      configureDynamicWifiIpStatus,
      configureStaticWifiIpStatus,
      configureLteModemStatus,
      currentNetworkConfigListStatus,
      currentNetworkConfigList
    } = this.state;
    if (configureDynamicIpStatus !== prevState.configureDynamicIpStatus) {
      if (configureDynamicIpStatus === "SUCCESS") {
        window.alert("Dynamic Ip is updated");
        this.props.history.push("./status");
      } else if (configureDynamicIpStatus === "FAIL") {
        window.alert("Updating Dynamic Ip is failed");
      }
    }

    if (configureStaticIpStatus !== prevState.configureStaticIpStatus) {
      if (configureStaticIpStatus === "SUCCESS") {
        window.alert("Static Ip is updated");
        this.props.history.push("./status");
      } else if (configureStaticIpStatus === "FAIL") {
        window.alert("Updating Static Ip is failed");
      }
    }

    if (
      configureDynamicWifiIpStatus !== prevState.configureDynamicWifiIpStatus
    ) {
      if (configureDynamicWifiIpStatus === "SUCCESS") {
        window.alert("Dynamic WiFi Ip is updated");
        this.props.history.push("./status");
      } else if (configureDynamicWifiIpStatus === "FAIL") {
        window.alert("Updating Dynamic WiFi Ip is failed");
      }
    }

    if (configureStaticWifiIpStatus !== prevState.configureStaticWifiIpStatus) {
      if (configureStaticWifiIpStatus === "SUCCESS") {
        window.alert("Static WiFi Ip is updated");
        this.props.history.push("./status");
      } else if (configureStaticWifiIpStatus === "FAIL") {
        window.alert("Updating Static WiFi Ip is failed");
      }
    }

    if (configureLteModemStatus !== prevState.configureLteModemStatus) {
      if (configureLteModemStatus === "SUCCESS") {
        window.alert("LTE Modem APN is updated");
        this.props.history.push("./status");
      } else if (configureLteModemStatus === "FAIL") {
        window.alert("Updating LTE Modem APN is failed");
      }
    }

    if (
      currentNetworkConfigListStatus !==
      prevState.currentNetworkConfigListStatus
    ) {
      this.setState({
        currentNetworkConfigList,
        currentNetworkConfigListStatus,
        isLoaded: true
      });
    }
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  postConfigureDynamicIp = async (user, pass) => {
    const { configureDynamicIpApiRequest } = this.props;
    // const payload = formatRequestParams.simplePostRequest(user, pass);
    const payload = {
      user: user,
      pass: pass
    };

    await configureDynamicIpApiRequest(payload);
  };

  postConfigureStaticIp = async (
    user,
    pass,
    ipAddress,
    subnetMask,
    staticGateWay,
    staticPreferredDNS,
    staticAlternateDNS
  ) => {
    const { configureStaticIpApiRequest } = this.props;

    const payload = {
      user,
      pass,
      ipAddress,
      subnetMask,
      gateway: staticGateWay,
      preferredDNS: staticPreferredDNS,
      alternateDNS: staticAlternateDNS
    };

    await configureStaticIpApiRequest(payload);
  };

  postConfigureDynamicWifiIp = async (user, pass, wifiSSID, wifiPass) => {
    const { configureDynamicWifiIpApiRequest } = this.props;
    // const payload = formatRequestParams.simplePostRequest(user, pass);
    const payload = {
      user: user,
      pass: pass,
      wifiSSID: wifiSSID,
      wifiPass: wifiPass
    };

    await configureDynamicWifiIpApiRequest(payload);
  };

  postConfigureStaticWifiIp = async (
    user,
    pass,
    staticWifiIp,
    wifiSubnetMask,
    staticWifiGateWay,
    staticWifiPreferredDNS,
    staticWifiAlternateDNS,
    wifiSSID,
    wifiPass
  ) => {
    const { configureStaticWifiIpApiRequest } = this.props;
    const payload = {
      user,
      pass,
      ipAddress: staticWifiIp,
      subnetMask: wifiSubnetMask,
      gateway: staticWifiGateWay,
      preferredDNS: staticWifiPreferredDNS,
      alternateDNS: staticWifiAlternateDNS,
      wifiSSID,
      wifiPass
    };

    await configureStaticWifiIpApiRequest(payload);
  };

  postConfigureLteModem = async (user, pass, apn) => {
    const { configureLteModemApiRequest } = this.props;
    // const payload = formatRequestParams.configureLteModemRequest(
    //   user,
    //   pass,
    //   apn
    // );

    const payload = {
      user,
      pass,
      apn
    };

    await configureLteModemApiRequest(payload);
  };

  getCurrentNetworkConfigList = async (user, pass) => {
    const { currentNetworkConfigListApiRequest } = this.props;

    const payload = formatRequestParams.simpleRequest({ user, pass });

    await currentNetworkConfigListApiRequest(payload);
  };

  fetchIpConfigApis = () => {
    const { user, pass } = this.props.credential;

    try {
      this.getCurrentNetworkConfigList(user, pass);
    } catch (e) {
      console.log(e);
      this.setState({
        errorMessage: e
      });
    }
  };

  handleIpChange = (e, { value }) =>
    this.setState({ dynamicIpAddress: value === "LAN_DYNAMIC" ? true : false });
  handleWifiIpChange = (e, { value }) =>
    this.setState({ dynamicWifiIpAddress: value });
  handleLetModem = (e, { value }) => this.setState({ letModem: value });

  handleStaticIpChange = async e =>
    await this.setState({ staticIp: e.target.value.toString() });
  handleStaticGateWayChange = async e =>
    await this.setState({ staticGateWay: e.target.value.toString() });
  handleStaticPreferredDNSChange = async e =>
    await this.setState({ staticPreferredDNS: e.target.value.toString() });
  handleStaticAlternateDNSChange = async e =>
    await this.setState({ staticAlternateDNS: e.target.value.toString() });
  handleSubnetMaskChange = async (e, data) =>
    await this.setState({ subnetMask: data.value });

  handleWifiSsidChange = async (e, data) =>
    await this.setState({ wifiSSID: e.target.value });
  handleWifiPassChange = async (e, data) =>
    await this.setState({ wifiPass: e.target.value });
  handleStaticWifiIpChange = async e =>
    await this.setState({ staticWifiIp: e.target.value.toString() });
  handleStaticWifiGateWayChange = async e =>
    await this.setState({ staticWifiGateWay: e.target.value.toString() });
  handleStaticWifiPreferredDNSChange = async e =>
    await this.setState({ staticWifiPreferredDNS: e.target.value.toString() });
  handleStaticWifiAlternateDNSChange = async e =>
    await this.setState({ staticWifiAlternateDNS: e.target.value.toString() });
  handleWifiSubnetMaskChange = async (e, data) =>
    await this.setState({ wifiSubnetMask: data.value });

  handleApnChange = async (e, data) =>
    await this.setState({ apn: e.target.value.toString() });

  submitStaticIp = data => {
    const { user, pass } = this.props.credential;
    const {
      staticIp,
      subnetMask,
      staticGateWay,
      staticPreferredDNS,
      staticAlternateDNS,
      networkType
    } = this.state;

    try {
      if (networkType === "LAN_DYNAMIC") {
        this.postConfigureDynamicIp(user, pass);
      } else {
        this.postConfigureStaticIp(
          user,
          pass,
          staticIp,
          subnetMask,
          staticGateWay,
          staticPreferredDNS,
          staticAlternateDNS
        );
      }
    } catch (e) {
      window.alert(e);
    }
  };

  submitStaticWifiIp = data => {
    const { user, pass } = this.props.credential;
    const {
      staticWifiIp,
      wifiSubnetMask,
      staticWifiGateWay,
      staticWifiPreferredDNS,
      staticWifiAlternateDNS,
      wifiSSID,
      wifiPass,
      networkType
    } = this.state;
    try {
      if (networkType === "WIFI_DYNAMIC") {
        this.postConfigureDynamicWifiIp(user, pass, wifiSSID, wifiPass);
      } else {
        this.postConfigureStaticWifiIp(
          user,
          pass,
          staticWifiIp,
          wifiSubnetMask,
          staticWifiGateWay,
          staticWifiPreferredDNS,
          staticWifiAlternateDNS,
          wifiSSID,
          wifiPass
        );
      }
    } catch (e) {
      window.alert(e);
    }
  };

  submitLteModem = data => {
    const { user, pass } = this.props.credential;
    const { letModem, apn } = this.state;

    try {
      if (!letModem) {
        this.postConfigureLteModem(user, pass, apn);
      }
    } catch (e) {
      window.alert(e);
    }
  };

  render() {
    const { networkType } = this.state;
    const networkTypes = this.props.currentNetworkConfigList.reduce(
      (acc, it) => [
        ...acc,
        { key: it.id, text: it.network, value: it.network }
      ],
      []
    );
    if (this.mounted) {
      return (
        <Container className="bodyContainer" fluid>
          <Grid>
            <Grid.Row>
              <Grid.Column>
                <Card style={{ width: "100%" }}>
                  <Card.Content textAlign="center">
                    <Card.Header>
                      {STRINGS.ipConfig["ipConfigTitle"]()}
                    </Card.Header>
                    <Divider section />
                    <Dropdown
                      placeholder={STRINGS.ipConfig["ipConfigSelection"]()}
                      fluid
                      selection
                      onChange={(e, data) => {
                        this.setState({ networkType: data.value });
                      }}
                      options={networkTypes}
                    />
                  </Card.Content>
                </Card>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>
                {networkType.includes("LAN")
                  ? this._renderIpConfiguration()
                  : ""}
                {networkType.includes("WIFI")
                  ? this._renderWifiIpConfiguration()
                  : ""}
                {networkType.includes("LTE") ? this._renderLteModem() : ""}
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Container>
      );
    } else {
      return this._renderLoader();
    }
  }

  _renderIpConfiguration = () => {
    const {
      subnetMasks,
      staticIp,
      staticGateWay,
      staticPreferredDNS,
      staticAlternateDNS,
      networkType,
      styles
    } = this.state;

    return (
      <Card style={styles.card}>
        <Card.Content>
          <Form>
            <Form.Field>
              <Checkbox
                radio
                label="Obatain an IP address automatically"
                name="checkboxRadioGroup"
                value={networkType}
                checked={networkType === "LAN_DYNAMIC" ? true : false}
                disabled={networkType === "LAN_DYNAMIC" ? false : true}
                onChange={this.handleIpChange}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                radio
                label="Use the following IP address"
                name="checkboxRadioGroup"
                value={networkType}
                checked={networkType === "LAN_DYNAMIC" ? false : true}
                disabled={networkType === "LAN_DYNAMIC" ? true : false}
                onChange={this.handleIpChange}
              />
            </Form.Field>
            <Form.Field>
              <Dimmer.Dimmable
                as={Segment}
                dimmed={networkType === "LAN_DYNAMIC"}
                className="inputSegment"
                style={{ justifyContent: "center" }}
              >
                <Dimmer
                  active={networkType === "LAN_DYNAMIC"}
                  inverted
                  className="inputSegment"
                />
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={5}>
                      <Header as="h5">IP address</Header>
                    </Grid.Column>
                    <Grid.Column width={11}>
                      <Input
                        className="ifConfig"
                        value={staticIp}
                        onChange={e => this.handleStaticIpChange(e)}
                      />
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row style={{ marginTop: "1em" }}>
                    <Grid.Column width={5}>
                      <Header as="h5">Subnet mask</Header>
                    </Grid.Column>
                    <Grid.Column width={11}>
                      <Dropdown
                        clearable
                        options={subnetMasks}
                        selection
                        style={{ marginLeft: "2em" }}
                        onChange={(e, data) =>
                          this.handleSubnetMaskChange(e, data)
                        }
                      />
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row style={styles.formField}>
                    <Grid.Column width={5}>
                      <Header as="h5">Default gateway</Header>
                    </Grid.Column>
                    <Grid.Column width={11}>
                      <Input
                        className="ifConfig"
                        value={staticGateWay}
                        onChange={e => this.handleStaticGateWayChange(e)}
                      />
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row style={styles.formField}>
                    <Grid.Column width={5}>
                      <Header as="h5">Preferred DNS</Header>
                    </Grid.Column>
                    <Grid.Column width={11}>
                      <Input
                        className="ifConfig"
                        value={staticPreferredDNS}
                        onChange={e => this.handleStaticPreferredDNSChange(e)}
                      />
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row style={styles.formField}>
                    <Grid.Column width={5}>
                      <Header as="h5">Alternate DNS</Header>
                    </Grid.Column>
                    <Grid.Column width={11}>
                      <Input
                        className="ifConfig"
                        value={staticAlternateDNS}
                        onChange={e => this.handleStaticAlternateDNSChange(e)}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Dimmer.Dimmable>
            </Form.Field>
          </Form>

          <Grid className="submitGrid">
            <Grid.Row className="submitRow">
              <Button
                color="green"
                onClick={(e, data) => this.submitStaticIp(data)}
              >
                Submit
              </Button>
            </Grid.Row>
          </Grid>
        </Card.Content>
      </Card>
    );
  };
  _renderWifiIpConfiguration = () => {
    const { networkType, styles } = this.state;
    return (
      <Card style={styles.card}>
        <Card.Content>
          <Form>
            <Form.Field>
              <Checkbox
                radio
                label="Obatain an WiFi IP address automatically"
                name="checkboxRadioGroup"
                value={networkType}
                checked={networkType === "WIFI_DYNAMIC" ? true : false}
                disabled={networkType === "WIFI_DYNAMIC" ? false : true}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                radio
                label="Use the following WiFi IP address"
                name="checkboxRadioGroup"
                value={networkType}
                checked={networkType === "WIFI_DYNAMIC" ? false : true}
                disabled={networkType === "WIFI_DYNAMIC" ? true : false}
                onChange={this.handleWifiIpChange}
              />
            </Form.Field>
            <Form.Field>
              {networkType === "WIFI_DYNAMIC"
                ? this._renderDynamicWifiIpConfiguration()
                : this._renderStaticWifiConfiguration()}
            </Form.Field>
          </Form>
          <Grid className="submitGrid">
            <Grid.Row className="submitRow">
              <Button
                color="green"
                onClick={(e, data) => this.submitStaticWifiIp(data)}
              >
                Submit
              </Button>
            </Grid.Row>
          </Grid>
        </Card.Content>
      </Card>
    );
  };
  _renderLteModem = () => {
    const { apn, styles } = this.state;

    return (
      <Card style={styles.card}>
        <Card.Content>
          <Form>
            <Form.Field>
              <Checkbox
                radio
                label="Set APN automatically"
                name="checkboxRadioGroup"
                value={true}
                checked={this.state.letModem === true}
                onChange={this.handleLetModem}
              />
            </Form.Field>
            <Form.Field>
              <Checkbox
                radio
                label="Use the following LTE Modem APN"
                name="checkboxRadioGroup"
                value={false}
                checked={this.state.letModem === false}
                onChange={this.handleLetModem}
              />
            </Form.Field>
            <Form.Field>
              <Dimmer.Dimmable as={Segment} dimmed={this.state.letModem}>
                <Dimmer active={this.state.letModem} inverted />
                <Grid>
                  <Grid.Row>
                    <Grid.Column width={5}>
                      <Header as="h5">APN</Header>
                    </Grid.Column>
                    <Grid.Column width={11}>
                      <Input
                        className="ifConfig"
                        value={apn}
                        onChange={e => this.handleApnChange(e)}
                      />
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Dimmer.Dimmable>
            </Form.Field>
          </Form>
          <Grid className="submitGrid">
            <Grid.Row className="submitRow">
              <Button
                color="green"
                onClick={(e, data) => this.submitLteModem(data)}
              >
                Submit
              </Button>
            </Grid.Row>
          </Grid>
        </Card.Content>
      </Card>
    );
  };
  _renderDynamicWifiIpConfiguration = () => {
    const { wifiSSID, wifiPass, styles } = this.state;
    return (
      <Segment className="inputSegment">
        <Grid>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">WiFi SSID</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={wifiSSID}
                onChange={e => this.handleWifiSsidChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">WiFi Password</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={wifiPass}
                onChange={e => this.handleWifiPassChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  };
  _renderStaticWifiConfiguration = () => {
    const {
      subnetMasks,
      staticWifiIp,
      staticWifiGateWay,
      staticWifiPreferredDNS,
      staticWifiAlternateDNS,
      wifiSSID,
      wifiPass,
      styles
    } = this.state;

    return (
      <Segment className="inputSegment">
        <Grid>
          <Grid.Row>
            <Grid.Column width={5}>
              <Header as="h5">Wifi IP address</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={staticWifiIp}
                onChange={e => this.handleStaticWifiIpChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">Subnet mask</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Dropdown
                clearable
                options={subnetMasks}
                selection
                style={{ marginLeft: "2em" }}
                onChange={(e, data) => this.handleWifiSubnetMaskChange(e, data)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">Default Wifi Gateway</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={staticWifiGateWay}
                onChange={e => this.handleStaticWifiGateWayChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">Preferred Wifi DNS</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={staticWifiPreferredDNS}
                onChange={e => this.handleStaticWifiPreferredDNSChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">Alternate Wifi DNS</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={staticWifiAlternateDNS}
                onChange={e => this.handleStaticWifiAlternateDNSChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">WiFi SSID</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={wifiSSID}
                onChange={e => this.handleWifiSsidChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
          <Grid.Row style={styles.formField}>
            <Grid.Column width={5}>
              <Header as="h5">WiFi Password</Header>
            </Grid.Column>
            <Grid.Column width={11}>
              <Input
                className="ifConfig"
                value={wifiPass}
                onChange={e => this.handleWifiPassChange(e)}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  };
  _renderLoader = () => {
    return (
      <Dimmer active inverted>
        <Loader inverted>Loading</Loader>
      </Dimmer>
    );
  };
}

const mapStateToProps = state => {
  return {
    credential: getAccountCredentials(state),
    configureDynamicIp: getConfigureDynamicIp(state),
    configureDynamicIpStatus: getConfigureDynamicIpStatus(state),
    configureStaticIp: getConfigureStaticIp(state),
    configureStaticIpStatus: getConfigureStaticIpStatus(state),
    configureDynamicWifiIp: getConfigureDynamicWifiIp(state),
    configureDynamicWifiIpStatus: getConfigureDynamicWifiIpStatus(state),
    configureStaticWifiIp: getConfigureStaticWifiIp(state),
    configureStaticWifiIpStatus: getConfigureStaticWifiIpStatus(state),
    configureLteModem: getConfigureLteModem(state),
    configureLteModemStatus: getConfigureLteModemStatus(state),
    currentNetworkConfigList: getCurrentNetworkConfigList(state),
    currentNetworkConfigListStatus: getCurrentNetworkConfigListStatus(state)
  };
};

const mapDispatchToProps = () => {
  return {
    configureDynamicIpApiRequest: actions.configureDynamicIpApiRequest,
    configureStaticIpApiRequest: actions.configureStaticIpApiRequest,
    configureDynamicWifiIpApiRequest: actions.configureDynamicWifiIpApiRequest,
    configureStaticWifiIpApiRequest: actions.configureStaticWifiIpApiRequest,
    configureLteModemApiRequest: actions.configureLteModemApiRequest,
    currentNetworkConfigListApiRequest:
      actions.currentNetworkConfigListApiRequest
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(IpConfig);

import React, { Component } from "react";
import { withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { actions } from "../../redux/actions/sensorsDetails.action";
import { formatRequestParams } from "../../utils/requestFormatter";
import {
  Button,
  Dropdown,
  Form,
  Icon,
  Image,
  Input,
  Segment,
  Header,
  Grid
} from "semantic-ui-react";
import "./style.scss";
import { COMPONENTS } from "../../components";
import { LANGUAGE_OPTIONS, LOGO_URL, ROUTES, STATUS } from "../../constants";
import { STRINGS } from "../../constants/strings";
import i18n from "../../i18n";
import { getSensorStatusList } from "../../redux/reducers/global.reducer";
import {
  getSensorStatusListStatus,
  getSensorStatusListError
} from "../../redux/reducers/sensorsDetails.reducer";

const { FormErrorMessage } = COMPONENTS;

class Login extends Component {
  state = {
    input: {
      username: "",
      password: ""
    },
    form: {
      showPassword: false
    },
    fetching_data: {
      sensor_status: false
    },
    api_error: {  
      sensor_status: {
        error: false,
        message: ""
      }
    },
    language: i18n.language,
    sensorStatusListStatus: undefined
  };

  constructor(props) {
    super(props);

    this.confirmSignin = this.confirmSignin.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleShowPassword = this.handleShowPassword.bind(this);
    this.handleLanguageChange = this.handleLanguageChange.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    const {
      sensorStatusListStatus,
      sensorStatusList
    } = props;

    if (sensorStatusListStatus !== undefined) {
      return {
        fetching_data: {
          sensor_status: sensorStatusListStatus === STATUS.LOADING
        },
        status: sensorStatusListStatus,
        sensorStatusListStatus
      };
    }

    if (sensorStatusListStatus !== state.sensorStatusListStatus) {
      return {
        ...state,
        sensorStatusList,
        sensorStatusListStatus
      };
    }

    return null;
  }

  componentDidUpdate(prevProps) {
    const {
      history,
      sensorStatusListStatus,
      sensorStatusListError
    } = this.props;

    if (sensorStatusListStatus !== prevProps.sensorStatusListStatus) {
      if (sensorStatusListStatus === STATUS.OK) {
        history.push(ROUTES.status);
      }

      if (sensorStatusListStatus === STATUS.ERR) {
        this.setState({
          api_error: {
            sensor_status: {
              error: sensorStatusListStatus === STATUS.ERR,
              message:
                sensorStatusListStatus === STATUS.ERR
                  ? sensorStatusListError
                  : ""
            }
          }
        });

        setTimeout(
          function() {
            this.setState(prevState => {
              let state = Object.assign({}, prevState);
              state.api_error.sensor_status.error = false;
              state.api_error.sensor_status.message = "";
              return state;
            });
            console.log(this.state);
          }.bind(this),
          3000
        );
      }
    }
    // if (sensorStatusListStatus === STATUS.OK) {
    //   history.push(ROUTES.status);
    //   // window.location.reload();
    //   // why push doesn't works?
    // }
  }

  confirmSignin = async () => {
    if (!this.state.fetching_data.sensor_status) {
      const { sensorStatusListApiRequest } = this.props;
      const payload = formatRequestParams.loginRequest({
        ...this.state.input
      });
      await sensorStatusListApiRequest(payload);
    }
    sessionStorage.setItem("language", this.state.language);
  };

  handleInputChange(field, data) {
    if (!this.state.fetching_data.sensor_status) {
      this.setState(prevState => {
        let state = Object.assign({}, prevState);
        state.input[field] = data.value;
        return state;
      });
    }
  }

  handleShowPassword() {
    if (!this.state.fetching_data.sensor_status) {
      this.setState(prevState => {
        let state = Object.assign({}, prevState);
        state.form["showPassword"] = !state.form["showPassword"];
        return state;
      });
    }
  }

  handleLanguageChange = async (e, data) => {
    this.props.i18n.changeLanguage(data.value);
    await this.setState({ language: data.value });
  };

  render() {
    const { api_error, sensorStatusListStatus } = this.state;

    return (
      <div className="login-container">
        <div className="loginBox">
          <Image
            src={LOGO_URL}
            size="medium"
            centered
            className="logo"
            style={{ marginBottom: "50px", marginTop: "50px" }}
          />
          {this._renderLoginForm()}
          {api_error.sensor_status.error && (
            <FormErrorMessage
              size="mini"
              text={this.props.t(api_error.sensor_status.message)}
              subText={STRINGS.error_message.try_again()}
            />
          )}
        </div>
      </div>
    );
  }

  _renderLoginForm() {
    const { api_error, fetching_data, form, input } = this.state;
    return (
      <Segment
        raised
        style={{ backgroundColor: "#666666", textAlign: "center" }}
      >
        <Header as="h2" style={{ color: "white" }}>
          Configuration Panel
        </Header>

        <Form inverted>
          <Form.Field inline>
            <Grid>
              <Grid.Row>
                <Grid.Column width={3}>
                  <label>{STRINGS.username()}</label>
                </Grid.Column>
                <Grid.Column width={13}>
                  <Input
                    placeholder={STRINGS.username()}
                    value={input["username"]}
                    onChange={(e, data) =>
                      this.handleInputChange("username", data)
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Form.Field>
          <Form.Field inline>
            <Grid>
              <Grid.Row>
                <Grid.Column width={3}>
                  <label>{STRINGS.password()}</label>
                </Grid.Column>
                <Grid.Column width={13}>
                  <Input
                    placeholder={STRINGS.password()}
                    value={input["password"]}
                    onChange={(e, data) =>
                      this.handleInputChange("password", data)
                    }
                    type={form["showPassword"] ? "text" : "password"}
                    icon={
                      <Icon
                        name="eye"
                        color={form["showPassword"] ? "blue" : "grey"}
                        onClick={this.handleShowPassword}
                        link
                      />
                    }
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Form.Field>

          <Form.Field inline>
            <Grid>
              <Grid.Row>
                <Grid.Column width={3}>
                  <label>{STRINGS.language()}</label>
                </Grid.Column>
                <Grid.Column width={13}>
                  <Dropdown
                    fluid
                    selection
                    options={LANGUAGE_OPTIONS}
                    defaultValue={i18n.language}
                    onChange={this.handleLanguageChange}
                  />
                </Grid.Column>
              </Grid.Row>
            </Grid>
          </Form.Field>

          <Button
            type="submit"
            floated="left"
            disabled={fetching_data.sensor_status}
            loading={fetching_data.sensor_status}
            onClick={this.confirmSignin}
          >
            {STRINGS.login().toUpperCase()}
          </Button>
        </Form>
      </Segment>
    );
  }
}

const mapStateToProps = state => {
  return {
    // availableSitesError: getAvailableSitesError(state),
    // availableSitesStatus: getAvailableSitesStatus(state),
    sensorStatusList: getSensorStatusList(state),
    sensorStatusListStatus: getSensorStatusListStatus(state),
    sensorStatusListError: getSensorStatusListError(state)
  };
};

const mapDispatchToProps = () => {
  return {
    sensorStatusListApiRequest: actions.sensorStatusListApiRequest
    // availableSitesApiRequest: actions.availableSitesApiRequest
  };
};

export default withTranslation()(
  connect(mapStateToProps, mapDispatchToProps())(Login)
);

import React, { Component } from "react";
import {
  Container,
  Header,
  Segment,
  Input
} from "semantic-ui-react";
import { connect } from "react-redux";
import { formatRequestParams } from "../../utils/requestFormatter";
import "../../styles/body.scss";

class SoftwareUpdate extends Component {
  state = {
    error: false
  };
  constructor(props) {
    super(props);
  }

  static getDerivedStateFromProps(props, state) {
    const {} = props;

    // if (visitorCountStatus !== state.visitorCountStatus) {
    //   return {
    //     ...state,
    //     visitorCount,
    //     visitorCountStatus,
    //     siteName: selectedSiteDetails.siteName,
    //     resolution: resolution,
    //     label: label,
    //   };

    return null;
  }

  componentDidMount = () => {
    this.mounted = true;
  };

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const {} = this.props;
    // if (resolution !== prevProps.resolution) {
    //   this.fetchVisitorVehiclesApis(siteId, currentDate, previousDate, resolution);
    // }
    // console.log(this.props);
    // console.log(this.state);
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  onChange(e) {
    let files = e.target.files;

    let reader = new FileReader();

    reader.readAsDataURL(files[0]);
    reader.onload = e => {
      console.warn("data", e.target);
    };
  }

  render() {
    return (
      <Container className="bodyContainer" fluid>
        <Segment className="bodySegment">
          <Header>This is Software Update Page</Header>

          <Segment textAlign="center">
            <Header>Upload a File</Header>
            <Input
              type="file"
              className="file"
              onChange={e => this.onChange(e)}
            />
          </Segment>
        </Segment>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = () => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps())(SoftwareUpdate);

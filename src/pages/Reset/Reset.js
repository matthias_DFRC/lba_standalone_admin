import React, { Component } from "react";
import { Container, Header, Button, Segment, Divider } from "semantic-ui-react";
import { connect } from "react-redux";
import { formatRequestParams } from "../../utils/requestFormatter";
import "../../styles/body.scss";
import {
  getAccountCredentials,
  getReset
} from "../../redux/reducers/global.reducer";
import { getResetStatus } from "../../redux/reducers/reset.reducer";
import { actions } from "../../redux/actions/reset.action";
import { STRINGS } from "../../constants/strings";

class Reset extends Component {
  state = {
    error: false
  };
  constructor(props) {
    super(props);
  }

  static getDerivedStateFromProps(props, state) {
    const { reset, resetStatus } = props;

    if (resetStatus !== state.resetStatus) {
      return {
        ...state,
        reset,
        resetStatus
      };
    }
    return null;
  }

  componentDidMount = () => {
    this.mounted = true;
  };

  async componentDidUpdate(prevProps, prevState, snapshot) {
    const {} = this.props;
    // if (resolution !== prevProps.resolution) {
    //   this.fetchVisitorVehiclesApis(siteId, currentDate, previousDate, resolution);
    // }
    // console.log(this.props);
    // console.log(this.state);
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error,
      errorInfo
    });
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  postReset = async (user, pass) => {
    const { resetApiRequest } = this.props;
    // const payload = formatRequestParams.simpleRequest(user, pass);
    const payload = {
      user,
      pass
    };

    await resetApiRequest(payload);
    window.alert("The system is going to reset. Please login again");
    sessionStorage.clear();
    this.props.history.push("../");
  };

  render() {
    const { user, pass } = this.props.credential;

    return (
      <Container className="bodyContainer" fluid>
        <Segment textAlign="center" style={{ marginLeft: "5em" }}>
          <Header as="h2">
            {STRINGS.reset["resetTitle"]()}
            <Header.Subheader>
              {STRINGS.reset["resetBody1"]()}
              <br />
              {STRINGS.reset["resetBody2"]()}
            </Header.Subheader>
          </Header>
          <Divider section />
          <Button.Group>
            <Button
              onClick={() => {
                this.props.history.push("./status");
              }}
            >
              No
            </Button>
            <Button.Or />
            <Button positive onClick={() => this.postReset(user, pass)}>
              Yes
            </Button>
          </Button.Group>
        </Segment>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    credential: getAccountCredentials(state),
    reset: getReset(state),
    resetStatus: getResetStatus(state)
  };
};

const mapDispatchToProps = () => {
  return {
    resetApiRequest: actions.resetApiRequest
  };
};

export default connect(mapStateToProps, mapDispatchToProps())(Reset);

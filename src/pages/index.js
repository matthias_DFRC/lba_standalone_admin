import Login from "./LoginPage/Login";
import Status from "./StatusPage/Status";
import Reset from "./Reset/Reset";
import Restart from "./Restart/Restart";
import IpConfig from "./IpConfig/IpConfig";
import SoftwareUpdate from "./SoftwareUpdate/SoftwareUpdate";
import SensorsDetails from './SensorsDetails/SensorsDetails';

export default {
  Login,
  Status,
  Reset,
  Restart,
  IpConfig,
  SoftwareUpdate,
  SensorsDetails,
};

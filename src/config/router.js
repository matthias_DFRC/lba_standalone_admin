import React, { Component } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";

import PAGES from "../pages";
import { COMPONENTS } from "../components";
import { ROUTES } from "../constants";
import { userAccount } from "../utils/userAccountUtils";

const {
  Login,
  Status,
  Reset,
  Restart,
  IpConfig,
  SoftwareUpdate,
  SensorsDetails
} = PAGES;
const { NavigationMenu } = COMPONENTS;

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        userAccount.isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: ROUTES.login, state: { from: props.location } }}
          />
        )
      }
    />
  );
};

const PublicRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        !userAccount.isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: ROUTES.status, state: { from: props.location } }}
          />
        )
      }
    />
  );
};

const LoginContainer = () => (
  <PublicRoute path={ROUTES.login} component={Login} />
);

const DefaultContainer = props => (
  <div>
    <NavigationMenu history={props.history} />
    <PrivateRoute path={ROUTES.status} component={Status} />
    <PrivateRoute path={ROUTES.ipConfig} component={IpConfig} />
    <PrivateRoute path={ROUTES.restart} component={Restart} />
    <PrivateRoute path={ROUTES.reset} component={Reset} />
    <PrivateRoute path={ROUTES.softwareUpdate} component={SoftwareUpdate} />
    <PrivateRoute path={ROUTES.sensorsDetails} component={SensorsDetails} />
    <Redirect to={ROUTES.status} />
  </div>
);

export default class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path={ROUTES.login} component={LoginContainer} />
          <Route path="/pages/*" component={DefaultContainer} />
          <Redirect to="/" />
        </Switch>
      </BrowserRouter>
    );
  }
}

import i18n from '../i18n';

export const STRINGS = {
  menu: {
    status: () => i18n.t('status'),
    ipConfig: () => i18n.t('ipConfig'),
    restart: () => i18n.t('restart'),
    reset: () => i18n.t('reset'),
    softwareUpdate: () => i18n.t('softwareUpdate'),
    sensorsDetails: () => i18n.t('sensorsDetails'),
  },

  username: () => i18n.t('Username'),
  password: () => i18n.t('Password'),
  language: () => i18n.t('Language'),
  login: () => i18n.t('Login'),

  error_message: {
    try_again: () => i18n.t('Please try again')
  },
  status:{
    sensorHealth: () => i18n.t('sensorHealth'),
    diskSize: () => i18n.t('diskSize'),
    cloudConnection: () => i18n.t('cloudConnection'),
    networkType: () => i18n.t('networkType'),
    localIp: () => i18n.t('localIp'),
    diskSizeStatus: () => i18n.t('diskSizeStatus'),
    veryGood: () => i18n.t('veryGood'),
    good: () => i18n.t('good'),
    low: () => i18n.t('low'),
    criticallyLow: () => i18n.t('criticallyLow'),
    available: () => i18n.t('available'),
    used: () => i18n.t('used'),
    size: () => i18n.t('size'),
  },
  restart: {
    restartTitle: () => i18n.t('restartTitle'),
    restartBody: () => i18n.t('restartBody'),
  },
  ipConfig: {
    ipConfigTitle: () => i18n.t('ipConfigTitle'),
    ipConfigSelection: () => i18n.t('ipConfigSelection'),
  },
  reset: {
    resetTitle: () => i18n.t('resetTitle'),
    resetBody1: () => i18n.t('resetBody1'),
    resetBody2: () => i18n.t('resetBody2'),
  },
  sensorDetails: {
    online: () => i18n.t('online'),
  }
};

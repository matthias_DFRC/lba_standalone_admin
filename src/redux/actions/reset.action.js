export const types = {
  RESET_API_REQUESTED: "LBA_LORA/GUI/RESET_API_REQUESTED",
  RESET_API_SUCCEEDED: "LBA_LORA/GUI/RESET_API_SUCCEEDED",
  RESET_API_FAILED: "LBA_LORA/GUI/RESET_API_FAILED",
};

export const actions = {
  resetApiRequest: payload => ({
    type: types.RESET_API_REQUESTED,
    payload
  }),
  resetApiSuccess: data => ({
    type: types.RESET_API_SUCCEEDED,
    data
  }),
  resetApiFailure: error => ({
    type: types.RESET_API_FAILED,
    error
  }),
};

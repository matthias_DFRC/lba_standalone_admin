export const types = {
  SENSOR_STATUS_LIST_API_REQUESTED: "LBA_LORA/GUI/SENSOR_STATUS_LIST_API_REQUESTED",
  SENSOR_STATUS_LIST_API_SUCCEEDED: "LBA_LORA/GUI/SENSOR_STATUS_LIST_API_SUCCEEDED",
  SENSOR_STATUS_LIST_API_FAILED: "LBA_LORA/GUI/SENSOR_STATUS_LIST_API_FAILED",
};

export const actions = {
  sensorStatusListApiRequest: payload => ({
    type: types.SENSOR_STATUS_LIST_API_REQUESTED,
    payload
  }),
  sensorStatusListApiSuccess: data => ({
    type: types.SENSOR_STATUS_LIST_API_SUCCEEDED,
    data
  }),
  sensorStatusListApiFailure: error => ({
    type: types.SENSOR_STATUS_LIST_API_FAILED,
    error
  }),
};

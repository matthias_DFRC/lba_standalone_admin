export const types = {
  AVAILABLE_SITES_API_REQUESTED:
    "LBA_MASTER/LOGIN/AVAILABLE_SITES_API_REQUESTED",
  AVAILABLE_SITES_API_SUCCEEDED:
    "LBA_MASTER/LOGIN/AVAILABLE_SITES_API_SUCCEEDED",
  AVAILABLE_SITES_API_FAILED: "LBA_MASTER/LOGIN/AVAILABLE_SITES_API_FAILED",
};

export const actions = {
  availableSitesApiRequest: payload => ({
    type: types.AVAILABLE_SITES_API_REQUESTED,
    payload
  }),
  availableSitesApiSuccess: () => ({
    type: types.AVAILABLE_SITES_API_SUCCEEDED
  }),
  availableSitesApiFailure: error => ({
    type: types.AVAILABLE_SITES_API_FAILED,
    error
  }),
};

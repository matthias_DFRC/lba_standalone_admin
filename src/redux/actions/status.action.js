export const types = {
  SENSOR_STATUS_API_REQUESTED: "LBA_LORA/GUI/SENSOR_STATUS_API_REQUESTED",
  SENSOR_STATUS_API_SUCCEEDED: "LBA_LORA/GUI/SENSOR_STATUS_API_SUCCEEDED",
  SENSOR_STATUS_API_FAILED: "LBA_LORA/GUI/SENSOR_STATUS_API_FAILED",
  SAP_COUNT_API_REQUESTED: "LBA_LORA/GUI/SAP_COUNT_API_REQUESTED",
  SAP_COUNT_API_SUCCEEDED: "LBA_LORA/GUI/SAP_COUNT_API_SUCCEEDED",
  SAP_COUNT_API_FAILED: "LBA_LORA/GUI/SAP_COUNT_API_FAILED",
  FREE_DISK_SIZE_API_REQUESTED: "LBA_LORA/GUI/FREE_DISK_SIZE_API_REQUESTED",
  FREE_DISK_SIZE_API_SUCCEEDED: "LBA_LORA/GUI/FREE_DISK_SIZE_API_SUCCEEDED",
  FREE_DISK_SIZE_API_FAILED: "LBA_LORA/GUI/FREE_DISK_SIZE_API_FAILED",
  LOCAL_IP_ADDRESS_API_REQUESTED: "LBA_LORA/GUI/LOCAL_IP_ADDRESS_API_REQUESTED",
  LOCAL_IP_ADDRESS_API_SUCCEEDED: "LBA_LORA/GUI/LOCAL_IP_ADDRESS_API_SUCCEEDED",
  LOCAL_IP_ADDRESS_API_FAILED: "LBA_LORA/GUI/LOCAL_IP_ADDRESS_API_FAILED",
  CLOUD_CONNECTION_API_REQUESTED: "LBA_LORA/GUI/CLOUD_CONNECTION_API_REQUESTED",
  CLOUD_CONNECTION_API_SUCCEEDED: "LBA_LORA/GUI/CLOUD_CONNECTION_API_SUCCEEDED",
  CLOUD_CONNECTION_API_FAILED: "LBA_LORA/GUI/CLOUD_CONNECTION_API_FAILED",
  CURRENT_NETWORK_CONFIG_API_REQUESTED: "CURRENT_NETWORK_CONFIG_API_REQUESTED",
  CURRENT_NETWORK_CONFIG_API_SUCCEEDED: "CURRENT_NETWORK_CONFIG_API_SUCCEEDED", 
  CURRENT_NETWORK_CONFIG_API_FAILED: "CURRENT_NETWORK_CONFIG_API_FAILED",
};

export const actions = {
  sensorStatusApiRequest: payload => ({
    type: types.SENSOR_STATUS_API_REQUESTED,
    payload
  }),
  sensorStatusApiSuccess: data => ({
    type: types.SENSOR_STATUS_API_SUCCEEDED,
    data
  }),
  sensorStatusApiFailure: error => ({
    type: types.SENSOR_STATUS_API_FAILED,
    error
  }),
  sapCountApiRequest: payload => ({
    type: types.SAP_COUNT_API_REQUESTED,
    payload
  }),
  sapCountApiSuccess: data => ({
    type: types.SAP_COUNT_API_SUCCEEDED,
    data
  }),
  sapCountApiFailure: error => ({
    type: types.SAP_COUNT_API_FAILED,
    error
  }),
  freeDiskSizeApiRequest: payload => ({
    type: types.FREE_DISK_SIZE_API_REQUESTED,
    payload
  }),
  freeDiskSizeApiSuccess: data => ({
    type: types.FREE_DISK_SIZE_API_SUCCEEDED,
    data
  }),
  freeDiskSizeApiFailure: error => ({
    type: types.FREE_DISK_SIZE_API_FAILED,
    error
  }),
  localIpAddressApiRequest: payload => ({
    type: types.LOCAL_IP_ADDRESS_API_REQUESTED,
    payload
  }),
  localIpAddressApiSuccess: data => ({
    type: types.LOCAL_IP_ADDRESS_API_SUCCEEDED,
    data
  }),
  localIpAddressApiFailure: error => ({
    type: types.LOCAL_IP_ADDRESS_API_FAILED,
    error
  }),
  cloudConnectionApiRequest: payload => ({
    type: types.CLOUD_CONNECTION_API_REQUESTED,
    payload
  }),
  cloudConnectionApiSuccess: data => ({
    type: types.CLOUD_CONNECTION_API_SUCCEEDED,
    data
  }),
  cloudConnectionApiFailure: error => ({
    type: types.CLOUD_CONNECTION_API_FAILED,
    error
  }),
  currentNetworkConfigApiRequest: payload => ({
    type: types.CURRENT_NETWORK_CONFIG_API_REQUESTED,
    payload
  }),
  currentNetworkConfigApiSuccess: payload => ({
    type: types.CURRENT_NETWORK_CONFIG_API_SUCCEEDED,
    payload
  }),
  currentNetworkConfigApiFailure: payload => ({
    type: types.CURRENT_NETWORK_CONFIG_API_FAILED,
    payload
  })
};

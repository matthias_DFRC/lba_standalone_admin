export const types = {
  LOAD_DATA_FROM_SESSION_STORAGE:
    "LBA_MASTER/GLOBAL/LOAD_DATA_FROM_SESSION_STORAGE",
  SAVE_AUTH_DETAILS: "LBA_MASTER/GLOBAL/SAVE_AUTH_DETAILS",
  SAVE_SENSOR_STATUS: "LBA_LORA/GLOBAL/SAVE_SENSOR_STATUS",
  SAVE_SAP_COUNT: "LBA_LORA/GLOBAL/SAVE_SAP_COUNT",
  SAVE_FREE_DISK_SIZE: "LBA_LORA/GLOBAL/SAVE_FREE_DISK_SIZE",
  SAVE_LOCAL_IP_ADDRESS: "LBA_LORA/GLOBAL/LOCAL_IP_ADDRESS",
  SAVE_CLOUD_CONNECTION: "LBA_LORA/GLOBAL/CLOUD_CONNECTION",
  SAVE_RESTART: "LBA_LORA/GLOBAL/RESTART",
  SAVE_RESET: "LBA_LORA/GLOBAL/RESET",
  SAVE_CONFIGURE_DYNAMIC_IP: "LBA_LORA/GLOBAL/CONFIGURE_DYNAMIC_IP",
  SAVE_CONFIGURE_STATIC_IP: "LBA_LORA/GLOBAL/CONFIGURE_STATIC_IP",
  SAVE_CONFIGURE_DYNAMIC_WIFI_IP: "LBA_LORA/GLOBAL/CONFIGURE_DYNAMIC_WIFI_IP",
  SAVE_CONFIGURE_STATIC_WIFI_IP: "LBA_LORA/GLOBAL/CONFIGURE_STATIC_WIFI_IP",
  SAVE_CONFIGURE_LTE_MODEM: "LBA_LORA/GLOBAL/CONFIGURE_LTE_MODEM",
  SAVE_SENSOR_STATUS_LIST: "LBA_LORA/GLOBAL/SENSOR_STATUS_LIST",
  SAVE_CURRENT_NETWORK_CONFIG: "LBA_LORA?GLOBAL/SAVE_CURRENT_NETWORK_CONFIG",
  SAVE_CURRENT_NETWORK_CONFIG_LIST: "LBA_LORA?GLOBAL/SAVE_CURRENT_NETWORK_CONFIG_LIST",
};

export const actions = {
  loadDataFromSessionStorage: () => ({
    type: types.LOAD_DATA_FROM_SESSION_STORAGE
  }),
  saveGlobalAuthDetails: (username, password, sensorId) => ({
    type: types.SAVE_AUTH_DETAILS,
    username,
    password,
    sensorId
  }),
  saveGlobalSensorStatus: response => ({
    type: types.SAVE_SENSOR_STATUS,
    response
  }),
  saveGlobalSapCount: response => ({
    type: types.SAVE_SAP_COUNT,
    response
  }),
  saveGlobalFreeDiskSize: response => ({
    type: types.SAVE_FREE_DISK_SIZE,
    response
  }),
  saveGlobalLocalIpAddress: response => ({
    type: types.SAVE_LOCAL_IP_ADDRESS,
    response
  }),
  saveGlobalCloudConnection: response => ({
    type: types.SAVE_CLOUD_CONNECTION,
    response
  }),
  saveGlobalRestart: response => ({
    type: types.SAVE_RESTART,
    response
  }),
  saveGlobalReset: response => ({
    type: types.SAVE_RESET,
    response
  }),
  saveGlobalConfigureDynamicIp: response => ({
    type: types.SAVE_CONFIGURE_DYNAMIC_IP,
    response
  }),
  saveGlobalConfigureStaticIp: response => ({
    type: types.SAVE_CONFIGURE_STATIC_IP,
    response
  }),
  saveGlobalConfigureDynamicWifiIp: response => ({
    type: types.SAVE_CONFIGURE_DYNAMIC_WIFI_IP,
    response
  }),
  saveGlobalConfigureStaticWifiIp: response => ({
    type: types.SAVE_CONFIGURE_STATIC_WIFI_IP,
    response
  }),
  saveGlobalConfigureLteModem: response => ({
    type: types.SAVE_CONFIGURE_LTE_MODEM,
    response
  }),
  saveGlobalSensorStatusList: response => ({
    type: types.SAVE_SENSOR_STATUS_LIST,
    response
  }),
  saveCurrentNetworkConfig: response => ({
    type: types.SAVE_CURRENT_NETWORK_CONFIG,
    response
  }),
  saveCurrentNetworkConfigList: response => ({
    type: types.SAVE_CURRENT_NETWORK_CONFIG_LIST,
    response
  })
};

export const types = {
  RESTART_API_REQUESTED: "LBA_LORA/GUI/RESTART_API_REQUESTED",
  RESTART_API_SUCCEEDED: "LBA_LORA/GUI/RESTART_API_SUCCEEDED",
  RESTART_API_FAILED: "LBA_LORA/GUI/RESTART_API_FAILED",
};

export const actions = {
  restartApiRequest: payload => ({
    type: types.RESTART_API_REQUESTED,
    payload
  }),
  restartApiSuccess: data => ({
    type: types.RESTART_API_SUCCEEDED,
    data
  }),
  restartApiFailure: error => ({
    type: types.RESTART_API_FAILED,
    error
  }),
};

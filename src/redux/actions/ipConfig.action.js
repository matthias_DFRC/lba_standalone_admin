export const types = {
  CONFIGURE_DYNAMIC_IP_API_REQUESTED: "LBA_LORA/GUI/CONFIGURE_DYNAMIC_IP_API_REQUESTED",
  CONFIGURE_DYNAMIC_IP_API_SUCCEEDED: "LBA_LORA/GUI/CONFIGURE_DYNAMIC_IP_API_SUCCEEDED",
  CONFIGURE_DYNAMIC_IP_API_FAILED: "LBA_LORA/GUI/CONFIGURE_DYNAMIC_IP_API_FAILED",
  CONFIGURE_STATIC_IP_API_REQUESTED: "LBA_LORA/GUI/CONFIGURE_STATIC_IP_API_REQUESTED",
  CONFIGURE_STATIC_IP_API_SUCCEEDED: "LBA_LORA/GUI/CONFIGURE_STATIC_IP_API_SUCCEEDED",
  CONFIGURE_STATIC_IP_API_FAILED: "LBA_LORA/GUI/CONFIGURE_STATIC_IP_API_FAILED",
  CONFIGURE_DYNAMIC_WIFI_IP_API_REQUESTED: "LBA_LORA/GUI/CONFIGURE_DYNAMIC_WIFI_IP_API_REQUESTED",
  CONFIGURE_DYNAMIC_WIFI_IP_API_SUCCEEDED: "LBA_LORA/GUI/CONFIGURE_DYNAMIC_WIFI_IP_API_SUCCEEDED",
  CONFIGURE_DYNAMIC_WIFI_IP_API_FAILED: "LBA_LORA/GUI/CONFIGURE_DYNAMIC_WIFI_IP_API_FAILED",
  CONFIGURE_STATIC_WIFI_IP_API_REQUESTED: "LBA_LORA/GUI/CONFIGURE_STATIC_WIFI_IP_API_REQUESTED",
  CONFIGURE_STATIC_WIFI_IP_API_SUCCEEDED: "LBA_LORA/GUI/CONFIGURE_STATIC_WIFI_IP_API_SUCCEEDED",
  CONFIGURE_STATIC_WIFI_IP_API_FAILED: "LBA_LORA/GUI/CONFIGURE_STATIC_WIFI_IP_API_FAILED",
  CONFIGURE_LTE_MODEM_API_REQUESTED: "LBA_LORA/GUI/CONFIGURE_LTE_MODEM_API_REQUESTED",
  CONFIGURE_LTE_MODEM_API_SUCCEEDED: "LBA_LORA/GUI/CONFIGURE_LTE_MODEM_API_SUCCEEDED",
  CONFIGURE_LTE_MODEM_API_FAILED: "LBA_LORA/GUI/CONFIGURE_LTE_MODEM_API_FAILED",
  CURRENT_NETWORK_CONFIG_LIST_API_REQUESTED: "CURRENT_NETWORK_CONFIG_LIST_API_REQUESTED",
  CURRENT_NETWORK_CONFIG_LIST_API_SUCCEEDED: "CURRENT_NETWORK_CONFIG_LIST_API_SUCCEEDED", 
  CURRENT_NETWORK_CONFIG_LIST_API_FAILED: "CURRENT_NETWORK_CONFIG_LIST_API_FAILED",
};

export const actions = {
  configureDynamicIpApiRequest: payload => ({
    type: types.CONFIGURE_DYNAMIC_IP_API_REQUESTED,
    payload
  }),
  configureDynamicIpApiSuccess: data => ({
    type: types.CONFIGURE_DYNAMIC_IP_API_SUCCEEDED,
    data
  }),
  configureDynamicIpApiFailure: error => ({
    type: types.CONFIGURE_DYNAMIC_IP_API_FAILED,
    error
  }),
  configureStaticIpApiRequest: payload => ({
    type: types.CONFIGURE_STATIC_IP_API_REQUESTED,
    payload
  }),
  configureStaticIpApiSuccess: data => ({
    type: types.CONFIGURE_STATIC_IP_API_SUCCEEDED,
    data
  }),
  configureStaticIpApiFailure: error => ({
    type: types.CONFIGURE_STATIC_IP_API_FAILED,
    error
  }),
  configureDynamicWifiIpApiRequest: payload => ({
    type: types.CONFIGURE_DYNAMIC_WIFI_IP_API_REQUESTED,
    payload
  }),
  configureDynamicWifiIpApiSuccess: data => ({
    type: types.CONFIGURE_DYNAMIC_WIFI_IP_API_SUCCEEDED,
    data
  }),
  configureDynamicWifiIpApiFailure: error => ({
    type: types.CONFIGURE_DYNAMIC_WIFI_IP_API_FAILED,
    error
  }),
  configureStaticWifiIpApiRequest: payload => ({
    type: types.CONFIGURE_STATIC_WIFI_IP_API_REQUESTED,
    payload
  }),
  configureStaticWifiIpApiSuccess: data => ({
    type: types.CONFIGURE_STATIC_WIFI_IP_API_SUCCEEDED,
    data
  }),
  configureStaticWifiIpApiFailure: error => ({
    type: types.CONFIGURE_STATIC_WIFI_IP_API_FAILED,
    error
  }),
  configureLteModemApiRequest: payload => ({
    type: types.CONFIGURE_LTE_MODEM_API_REQUESTED,
    payload
  }),
  configureLteModemApiSuccess: data => ({
    type: types.CONFIGURE_LTE_MODEM_API_SUCCEEDED,
    data
  }),
  configureLteModemApiFailure: error => ({
    type: types.CONFIGURE_LTE_MODEM_API_FAILED,
    error
  }),
  currentNetworkConfigListApiRequest: payload => ({
    type: types.CURRENT_NETWORK_CONFIG_LIST_API_REQUESTED,
    payload
  }),
  currentNetworkConfigListApiSuccess: payload => ({
    type: types.CURRENT_NETWORK_CONFIG_LIST_API_SUCCEEDED,
    payload
  }),
  currentNetworkConfigListApiFailure: payload => ({
    type: types.CURRENT_NETWORK_CONFIG_LIST_API_FAILED,
    payload
  })
};

if (process.env.NODE_ENV === "production") {
  module.exports = require("./api-prod/reset");
} else {
  module.exports = require("./api-dev/reset");
}

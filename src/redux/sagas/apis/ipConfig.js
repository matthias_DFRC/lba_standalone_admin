if (process.env.NODE_ENV === 'production') {
  module.exports = require('./api-prod/ipConfig');
} else {
  module.exports = require('./api-dev/ipConfig');
}

import axios from "axios";

import { API, LOCAL_HOST_URL } from "../../../../constants";

export const sensorStatusListApi = payload => {
  const url = `${LOCAL_HOST_URL}/${API.SENSOR_STATUS_LIST}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};

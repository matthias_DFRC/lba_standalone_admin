import axios from "axios";

import { API, LOCAL_HOST_URL } from "../../../../constants";

export const configureDynamicIpApi = payload => {
  const url = `${LOCAL_HOST_URL}/${API.CONFIG_DYNAMIC_IP}`;

  const config = {
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Headers": "*"
    },
    params: { ...payload }
  };

  const data = {};

  return axios.post(url, null, config);
};

export const configureStaticIpApi = payload => {
  const url = `${LOCAL_HOST_URL}/${API.CONFIG_STATIC_IP}`;

  const config = {
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Headers": "*"
    },
    params: { ...payload }
  };

  const data = {};

  return axios.post(url, data, config);
};

export const configureDynamicWifiIpApi = payload => {
  const url = `${LOCAL_HOST_URL}/${API.CONFIG_DYNAMIC_WIFI_IP}`;

  const config = {
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Headers": "*"
    },
    params: { ...payload }
  };

  const data = {};

  return axios.post(url, null, config);
};

export const configureStaticWifiIpApi = payload => {
  const url = `${LOCAL_HOST_URL}/${API.CONFIG_STATIC_WIFI_IP}`;

  const config = {
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Headers": "*"
    },
    params: { ...payload }
  };

  const data = {};

  return axios.post(url, data, config);
};

export const configureLteModemApi = payload => {
  const url = `${LOCAL_HOST_URL}/${API.CONFIG_LET_MODEM}`;

  const config = {
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Headers": "*"
    },
    params: { ...payload }
  };

  const data = {};

  return axios.post(url, data, config);
};

export const currentNetworkConfigListApi = payload => {
  const url = `${LOCAL_HOST_URL}/${API.CURRENT_NETWORK_CONFIG_LIST}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};

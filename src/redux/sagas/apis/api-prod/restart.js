import axios from "axios";

import { API, LOCAL_HOST_URL } from "../../../../constants";

export const restartApi = payload => {
  const url = `/${API.RESTART}`;

  const config = {
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Headers": "*"
    },
    params: { ...payload }
  };

  const data = {};

  return axios.post(url, data, config);
};

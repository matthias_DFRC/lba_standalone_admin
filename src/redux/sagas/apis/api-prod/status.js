import axios from "axios";

import { API, LOCAL_HOST_URL } from "../../../../constants";

export const sensorStatusApi = payload => {
  // standalone server doesn't need domain!!!!
  // const url = `${LOCAL_HOST_URL}/${API.SENSOR_STATUS}`;
  const url = `/${API.SENSOR_STATUS}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};

export const sapCountApi = payload => {
  const url = `/${API.SAP_COUNT}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};

export const freeDiskSizeApi = payload => {
  const url = `/${API.FREE_DISK_SIZE}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};

export const localIpAddressApi = payload => {
  const url = `/${API.LOCAL_IP_ADDRESS}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};

export const cloudConnectionApi = payload => {
  const url = `/${API.CLOUD_CONNECTION}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};

export const currentNetworkConfigApi = payload => {
  const url = `/${API.CURRENT_NETWORK_CONFIG}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};
import axios from "axios";

import { API, MASTER_SERVER_URL, ADMIN_SERVER_URL } from "../../../constants";

export const availableSitesApi = payload => {
  const url = `${ADMIN_SERVER_URL}/${API.AVAILABLE_SITES}`;

  const config = {
    headers: {
      "Content-type": "application/json"
    },
    params: { ...payload }
  };

  return axios.get(url, config);
};


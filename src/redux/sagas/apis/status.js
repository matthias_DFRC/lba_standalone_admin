if (process.env.NODE_ENV === 'production') {
  module.exports = require('./api-prod/status');
} else {
  module.exports = require('./api-dev/status');
}

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./api-prod/sensorsDetails');
} else {
  module.exports = require('./api-dev/sensorsDetails');
}

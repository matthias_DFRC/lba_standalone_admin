if (process.env.NODE_ENV === 'production') {
  module.exports = require('./api-prod/restart');
} else {
  module.exports = require('./api-dev/restart');
}

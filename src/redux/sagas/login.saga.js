// import { put, takeLatest } from "redux-saga/effects";
// import { types, actions } from "../actions/sensorsDetails.action";
// import { actions as globalActions } from "../actions/global.action";
// import { userAccount } from "../../utils/userAccountUtils";
// import { sensorStatusListApi } from "./apis/sensorsDetails";

// function* sensorStatusListApiSaga(action) {
//   try {
//     const { payload } = action;
//     const response = yield sensorStatusListApi(payload);

//     yield put(
//       globalActions.saveGlobalAuthDetails(
//         payload.user,
//         payload.pass,
//         response.sensorId
//       )
//     );
//     userAccount.setLoginSessionInfo(
//       payload.user,
//       payload.pass,
//       response.sensorId
//     );

//     yield put(actions.sensorStatusListApiSuccess());
//   } catch (err) {
//     const { data } = err.response;
//     yield put(actions.sensorStatusListApiFailure(data.message));
//   }
// }

// export default function* watchLoginSaga() {
//   yield takeLatest(
//     types.SENSOR_STATUS_LIST_API_REQUESTED,
//     sensorStatusListApiSaga
//   );
// }

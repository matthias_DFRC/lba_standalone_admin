import { all, fork } from "redux-saga/effects";
import watchLoginSaga from "./login.saga";
import watchStatusSaga from "./status.saga";
import watchRestartSaga from "./restart.saga";
import watchResetSaga from "./reset.saga";
import watchIpConfigSaga from "./ipConfig.saga";
import watchSensorsDetailsSaga from "./sensorsDetails.saga";

export default function* rootSaga() {
  yield all([
    // fork(watchLoginSaga),
    fork(watchStatusSaga),
    fork(watchRestartSaga),
    fork(watchResetSaga),
    fork(watchIpConfigSaga),
    fork(watchSensorsDetailsSaga)
  ]);
}

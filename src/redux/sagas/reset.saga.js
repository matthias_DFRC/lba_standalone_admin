import { put, takeLatest } from "redux-saga/effects";
import { types, actions } from "../actions/reset.action";
import { actions as globalActions } from "../actions/global.action";
import { parseApiResponse } from "../../utils/responseParser";
import { userAccount } from "../../utils/userAccountUtils";
import { resetApi } from "./apis/reset";

function* restartApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield resetApi(payload);

    yield put(globalActions.saveGlobalReset(response.data));

    yield put(actions.resetApiSuccess(response.data));
  } catch (err) {
    // const { data } = err.response;
    yield put(actions.resetApiFailure(err));
  }
}

export default function* watchResetSaga() {
  yield takeLatest(types.RESET_API_REQUESTED, restartApiSaga);
}

import { put, takeLatest } from "redux-saga/effects";
import { types, actions } from "../actions/restart.action";
import { actions as globalActions } from "../actions/global.action";
import { parseApiResponse } from "../../utils/responseParser";
import { userAccount } from "../../utils/userAccountUtils";
import { restartApi } from "./apis/restart";

function* restartApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield restartApi(payload);

    yield put(globalActions.saveGlobalRestart(response.data));

    yield put(actions.restartApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.restartApiFailure(data.message));
  }
}

export default function* watchRestartSaga() {
  yield takeLatest(types.RESTART_API_REQUESTED, restartApiSaga);
}

import { put, takeLatest } from "redux-saga/effects";
import { types, actions } from "../actions/status.action";
import { actions as globalActions } from "../actions/global.action";
import { parseApiResponse } from "../../utils/responseParser";
import { userAccount } from "../../utils/userAccountUtils";
import {
  sensorStatusApi,
  sapCountApi,
  freeDiskSizeApi,
  localIpAddressApi,
  cloudConnectionApi,
  currentNetworkConfigApi
} from "./apis/status";

function* sensorStatusApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield sensorStatusApi(payload);

    yield put(globalActions.saveGlobalSensorStatus(response.data));

    yield put(actions.sensorStatusApiSuccess(response.data));

    // yield put(actions.sensorStatusApiSuccess());
  } catch (err) {
    const { data } = err.response;
    yield put(actions.sensorStatusApiFailure(data.message));
  }
}

function* sapCountApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield sapCountApi(payload);

    yield put(globalActions.saveGlobalSapCount(response.data));

    yield put(actions.sapCountApiSuccess(response.data));
  } catch (err) {
    // const { data } = err.response;
    yield put(actions.sapCountApiFailure(err));
  }
}

function* freeDiskSizeApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield freeDiskSizeApi(payload);

    yield put(globalActions.saveGlobalFreeDiskSize(response.data));

    yield put(actions.freeDiskSizeApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.freeDiskSizeApiFailure(data.message));
  }
}

function* localIpAddressApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield localIpAddressApi(payload);

    yield put(globalActions.saveGlobalLocalIpAddress(response.data));

    yield put(actions.localIpAddressApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.localIpAddressApiFailure(data.message));
  }
}

function* cloudConnectionApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield cloudConnectionApi(payload);

    yield put(globalActions.saveGlobalCloudConnection(response.data));

    yield put(actions.cloudConnectionApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.cloudConnectionApiFailure(data.message));
  }
}

function* currentNetworkConfigApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield currentNetworkConfigApi(payload);

    yield put(globalActions.saveCurrentNetworkConfig(response.data));

    yield put(actions.currentNetworkConfigApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.currentNetworkConfigApiFailure(data.message));
  }
}

export default function* watchStatusSaga() {
  yield takeLatest(types.SENSOR_STATUS_API_REQUESTED, sensorStatusApiSaga);
  yield takeLatest(types.SAP_COUNT_API_REQUESTED, sapCountApiSaga);
  yield takeLatest(types.FREE_DISK_SIZE_API_REQUESTED, freeDiskSizeApiSaga);
  yield takeLatest(types.LOCAL_IP_ADDRESS_API_REQUESTED, localIpAddressApiSaga);
  yield takeLatest(
    types.CLOUD_CONNECTION_API_REQUESTED,
    cloudConnectionApiSaga
  );
  yield takeLatest(
    types.CURRENT_NETWORK_CONFIG_API_REQUESTED,
    currentNetworkConfigApiSaga
  );
}

import { put, takeLatest } from "redux-saga/effects";
import { types, actions } from "../actions/sensorsDetails.action";
import { actions as globalActions } from "../actions/global.action";
import { parseApiResponse } from "../../utils/responseParser";
import { sensorStatusListApi } from "./apis/sensorsDetails";
import { userAccount } from "../../utils/userAccountUtils";

function* sensorStatusListApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield sensorStatusListApi(payload);
    yield put(globalActions.saveGlobalSensorStatusList(response.data));
    yield put(
      globalActions.saveGlobalAuthDetails(
        payload.user,
        payload.pass,
        response.data[0].sensorId
      )
    );
    userAccount.setLoginSessionInfo(
      payload.user,
      payload.pass,
      response.data[0].sensorId
    );

    yield put(actions.sensorStatusListApiSuccess(response.data));

    // yield put(actions.sensorStatusApiSuccess());
  } catch (err) {
    const { data } = err.response;
    yield put(actions.sensorStatusListApiFailure(data.message));
  }
}

export default function* watchSensorsDetailsSaga() {
  yield takeLatest(
    types.SENSOR_STATUS_LIST_API_REQUESTED,
    sensorStatusListApiSaga
  );
}

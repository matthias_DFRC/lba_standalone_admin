import { put, takeLatest } from "redux-saga/effects";
import { types, actions } from "../actions/ipConfig.action";
import { actions as globalActions } from "../actions/global.action";
import { parseApiResponse } from "../../utils/responseParser";
import {
  configureDynamicIpApi,
  configureStaticIpApi,
  configureDynamicWifiIpApi,
  configureStaticWifiIpApi,
  configureLteModemApi,
  currentNetworkConfigListApi
} from "./apis/ipConfig";

function* configureDynamicIpApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield configureDynamicIpApi(payload);

    yield put(globalActions.saveGlobalConfigureDynamicIp(response.data));

    yield put(actions.configureDynamicIpApiSuccess(response.data));
  } catch (err) {
    // const { data } = err.response;
    yield put(actions.configureDynamicIpApiFailure(err));
  }
}

function* configureStaticIpApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield configureStaticIpApi(payload);

    yield put(globalActions.saveGlobalConfigureStaticIp(response.data));

    yield put(actions.configureStaticIpApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.configureStaticIpApiFailure(data.message));
  }
}

function* configureDynamicWifiIpApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield configureDynamicWifiIpApi(payload);

    yield put(globalActions.saveGlobalConfigureDynamicWifiIp(response.data));

    yield put(actions.configureDynamicWifiIpApiSuccess(response.data));
  } catch (err) {
    // const { data } = err.response;
    yield put(actions.configureDynamicWifiIpApiFailure(err));
  }
}

function* configureStaticWifiIpApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield configureStaticWifiIpApi(payload);

    yield put(globalActions.saveGlobalConfigureStaticWifiIp(response.data));

    yield put(actions.configureStaticWifiIpApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.configureStaticWifiIpApiFailure(data.message));
  }
}

function* configureLteModemApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield configureLteModemApi(payload);

    yield put(globalActions.saveGlobalConfigureLteModem(response.data));

    yield put(actions.configureLteModemApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.configureLteModemApiFailure(data.message));
  }
}

function* currentNetworkConfigListApiSaga(action) {
  try {
    const { payload } = action;
    const response = yield currentNetworkConfigListApi(payload);

    yield put(globalActions.saveCurrentNetworkConfigList(response.data));

    yield put(actions.currentNetworkConfigListApiSuccess(response.data));
  } catch (err) {
    const { data } = err.response;
    yield put(actions.currentNetworkConfigListApiFailure(err));
  }
}

export default function* watchIpConfigSaga() {
  yield takeLatest(
    types.CONFIGURE_DYNAMIC_IP_API_REQUESTED,
    configureDynamicIpApiSaga
  );
  yield takeLatest(
    types.CONFIGURE_STATIC_IP_API_REQUESTED,
    configureStaticIpApiSaga
  );
  yield takeLatest(
    types.CONFIGURE_DYNAMIC_WIFI_IP_API_REQUESTED,
    configureDynamicWifiIpApiSaga
  );
  yield takeLatest(
    types.CONFIGURE_STATIC_WIFI_IP_API_REQUESTED,
    configureStaticWifiIpApiSaga
  );
  yield takeLatest(
    types.CONFIGURE_LTE_MODEM_API_REQUESTED,
    configureLteModemApiSaga
  );
  yield takeLatest(
    types.CURRENT_NETWORK_CONFIG_LIST_API_REQUESTED,
    currentNetworkConfigListApiSaga
  );
}

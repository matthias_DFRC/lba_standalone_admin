import { combineReducers } from "redux";
import globalReducer from "./global.reducer";
import loginReducer from "./login.reducer";
import statusReducer from "./status.reducer";
import restartReducer from "./restart.reducer";
import resetReducer from "./reset.reducer";
import ipConfigReducer from "./ipConfig.reducer";
import sensorsDetailsReducer from "./sensorsDetails.reducer";

export default combineReducers({
  globalReducer,
  loginReducer,
  statusReducer,
  restartReducer,
  resetReducer,
  ipConfigReducer,
  sensorsDetailsReducer
});

import Immutable from 'seamless-immutable';
import { types } from '../actions/login.action';
import { STATUS } from '../../constants';

const INITIAL_STATE = Immutable({
  status: {
    availableSites: undefined
  },
  errorMessage: undefined
});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.AVAILABLE_SITES_API_REQUESTED:
      return state.merge({
        status: { ...state.status, availableSites: STATUS.LOADING }
      });
    case types.AVAILABLE_SITES_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, availableSites: STATUS.OK }
      });
    case types.AVAILABLE_SITES_API_FAILED:
      return state.merge({
        status: { ...state.status, availableSites: STATUS.ERR },
        errorMessage: action.error
      });
    default:
      return state;
  }
};

export const getAvailableSitesStatus = state =>
  state.loginReducer.status.availableSites;

export const getAvailableSitesError = state => state.loginReducer.errorMessage;

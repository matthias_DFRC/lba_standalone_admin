import Immutable from "seamless-immutable";
import { types } from "../actions/ipConfig.action";
import { STATUS } from "../../constants";

const INITIAL_STATE = Immutable({
  status: {
    configureDynamicIpStatus: undefined,
    configureStaticIpStatus: undefined,
    configureDynamicWifiIpStatus: undefined,
    configureStaticWifiIpStatus: undefined,
    configureLteModemStatus: undefined,
    currentNetworkConfigListStatus: undefined,
  },
  errorMessage: undefined,
  configureDynamicIp: undefined,
  configureStaticIp: undefined,
  configureDynamicWifiIp: undefined,
  configureStaticWifiIp: undefined,
  configureLteModem: undefined,
  currentNetworkConfigList: undefined,
});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.CONFIGURE_DYNAMIC_IP_API_REQUESTED:
      return state.merge({
        status: { ...state.status, configureDynamicIpStatus: STATUS.LOADING }
      });
    case types.CONFIGURE_DYNAMIC_IP_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, configureDynamicIpStatus: STATUS.OK },
        configureDynamicIp: action.data
      });
    case types.CONFIGURE_DYNAMIC_IP_API_FAILED:
      return state.merge({
        status: { ...state.status, configureDynamicIpStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.CONFIGURE_STATIC_IP_API_REQUESTED:
      return state.merge({
        status: { ...state.status, configureStaticIpStatus: STATUS.LOADING }
      });
    case types.CONFIGURE_STATIC_IP_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, configureStaticIpStatus: STATUS.OK },
        configureStaticIp: action.data
      });
    case types.CONFIGURE_STATIC_IP_API_FAILED:
      return state.merge({
        status: { ...state.status, configureStaticIpStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.CONFIGURE_DYNAMIC_WIFI_IP_API_REQUESTED:
      return state.merge({
        status: {
          ...state.status,
          configureDynamicWifiIpStatus: STATUS.LOADING
        }
      });
    case types.CONFIGURE_DYNAMIC_WIFI_IP_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, configureDynamicWifiIpStatus: STATUS.OK },
        configureDynamicWifiIp: action.data
      });
    case types.CONFIGURE_DYNAMIC_WIFI_IP_API_FAILED:
      return state.merge({
        status: { ...state.status, configureDynamicWifiIpStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.CONFIGURE_STATIC_WIFI_IP_API_REQUESTED:
      return state.merge({
        status: { ...state.status, configureStaticWifiIpStatus: STATUS.LOADING }
      });
    case types.CONFIGURE_STATIC_WIFI_IP_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, configureStaticWifiIpStatus: STATUS.OK },
        configureStaticWifiIp: action.data
      });
    case types.CONFIGURE_STATIC_WIFI_IP_API_FAILED:
      return state.merge({
        status: { ...state.status, configureStaticWifiIpStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.CONFIGURE_LTE_MODEM_API_REQUESTED:
      return state.merge({
        status: { ...state.status, configureLteModemStatus: STATUS.LOADING }
      });
    case types.CONFIGURE_LTE_MODEM_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, configureLteModemStatus: STATUS.OK },
        configureLteModem: action.data
      });
    case types.CONFIGURE_LTE_MODEM_API_FAILED:
      return state.merge({
        status: { ...state.status, configureLteModemStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.CURRENT_NETWORK_CONFIG_LIST_API_REQUESTED:
      return state.merge({
        status: { ...state.status, currentNetworkConfigListStatus: STATUS.LOADING }
      });
    case types.CURRENT_NETWORK_CONFIG_LIST_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, currentNetworkConfigListStatus: STATUS.OK },
        currentNetworkConfigList: action.data
      });
    case types.CURRENT_NETWORK_CONFIG_LIST_API_FAILED:
      return state.merge({
        status: { ...state.status, currentNetworkConfigListStatus: STATUS.ERR },
        errorMessage: action.error
      });
    default:
      return state;
  }
};

export const getConfigureDynamicIpStatus = state =>
  state.ipConfigReducer.status.configureDynamicIpStatus;
export const getConfigureDynamicIpError = state =>
  state.ipConfigReducer.errorMessage;

export const getConfigureStaticIpStatus = state =>
  state.ipConfigReducer.status.configureStaticIpStatus;
export const getConfigureStaticIpError = state =>
  state.ipConfigReducer.errorMessage;

export const getConfigureDynamicWifiIpStatus = state =>
  state.ipConfigReducer.status.configureDynamicWifiIpStatus;
export const getConfigureDynamicWifiIpError = state =>
  state.ipConfigReducer.errorMessage;

export const getConfigureStaticWifiIpStatus = state =>
  state.ipConfigReducer.status.configureStaticWifiIpStatus;
export const getConfigureStaticWifiIpError = state =>
  state.ipConfigReducer.errorMessage;

export const getConfigureLteModemStatus = state =>
  state.ipConfigReducer.status.configureLteModemStatus;
export const getConfigureLteModemError = state =>
  state.ipConfigReducer.errorMessage;

  export const getCurrentNetworkConfigListStatus = state =>
  state.statusReducer.status.currentNetworkConfigListStatus;
export const getCurrentNetworkConfigListError = state => state.statusReducer.errorMessage;
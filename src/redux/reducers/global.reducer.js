import { types } from "../actions/global.action";
import Immutable from "seamless-immutable";
import { sessionstorage } from "../../utils/sessionstorage";
import { SESSION_DATA, STATUS } from "../../constants";

const INITIAL_STATE = Immutable({
  appStatus: undefined,
  username: undefined,
  password: undefined,
  sensorId: undefined,
  sensorStatus: undefined,
  sapCount: undefined,
  freeDiskSize: {
    availableDiskSize: undefined,
    diskSize: undefined,
    availableDiskSizePercent: undefined,
    usageDiskSizePercent: undefined
  },
  localIpAddress: undefined,
  cloudConnection: undefined,
  restart: undefined,
  reset: undefined,
  configureDynamicIp: undefined,
  configureStaticIp: undefined,
  configureDynamicWifiIp: undefined,
  configureStaticWifiIp: undefined,
  configureLteModem: undefined,
  sensorStatusList: undefined,
  currentNetworkConfig: {
    id: undefined,
    network: undefined,
    description: undefined,
    response: undefined
  },
  currentNetworkConfigList: [],
  error: false,
  errorMessage: undefined
});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.LOAD_DATA_FROM_SESSION_STORAGE:
      return state.merge({
        ...state,
        username: sessionstorage.get(SESSION_DATA.authentication)["username"],
        password: sessionstorage.get(SESSION_DATA.authentication)["password"],
        sensorId: sessionstorage.get(SESSION_DATA.authentication)["sensorId"],
        appStatus: STATUS.INITIALIZED
      });
    case types.SAVE_AUTH_DETAILS:
      return state.merge({
        ...state,
        username: action.username,
        password: action.password,
        sensorId: action.sensorId,
        appStatus: STATUS.INITIALIZED
      });
    case types.SAVE_SENSOR_STATUS:
      return state.merge({
        ...state,
        sensorStatus: action.response
      });
    case types.SAVE_SAP_COUNT:
      return state.merge({
        ...state,
        sapCount: action.response
      });
    case types.SAVE_FREE_DISK_SIZE:
      return state.merge({
        ...state,
        freeDiskSize: action.response
      });
    case types.SAVE_LOCAL_IP_ADDRESS:
      return state.merge({
        ...state,
        localIpAddress: action.response
      });
    case types.SAVE_CLOUD_CONNECTION:
      return state.merge({
        ...state,
        cloudConnection: action.response
      });
    case types.SAVE_RESTART:
      return state.merge({
        ...state,
        restart: action.response
      });
    case types.SAVE_RESET:
      return state.merge({
        ...state,
        reset: action.response
      });
    case types.SAVE_CONFIGURE_DYNAMIC_IP:
      return state.merge({
        ...state,
        configureDynamicIp: action.response
      });
    case types.SAVE_CONFIGURE_STATIC_IP:
      return state.merge({
        ...state,
        configureStaticIp: action.response
      });
    case types.SAVE_CONFIGURE_DYNAMIC_WIFI_IP:
      return state.merge({
        ...state,
        configureDynamicWifiIp: action.response
      });
    case types.SAVE_CONFIGURE_STATIC_WIFI_IP:
      return state.merge({
        ...state,
        configureStaticWifiIp: action.response
      });
    case types.SAVE_CONFIGURE_LTE_MODEM:
      return state.merge({
        ...state,
        configureLteModem: action.response
      });
    case types.SAVE_SENSOR_STATUS_LIST:
      return state.merge({
        ...state,
        sensorStatusList: action.response
      });
    case types.SAVE_CURRENT_NETWORK_CONFIG:
      return state.merge({
        ...state,
        currentNetworkConfig: action.response
      });
    case types.SAVE_CURRENT_NETWORK_CONFIG_LIST:
      return state.merge({
        ...state,
        currentNetworkConfigList: action.response
      });
    default:
      return state;
  }
};

export const getAccountCredentials = state => ({
  user: state.globalReducer.username,
  pass: state.globalReducer.password,
  sensorId: state.globalReducer.sensorId
});

export const getAppStatus = state => state.globalReducer.appStatus;

export const getSensorStatus = state => state.globalReducer.sensorStatus;
export const getSapCount = state => state.globalReducer.sapCount;
export const getFreeDiskSize = state => state.globalReducer.freeDiskSize;
export const getLocalIpAddress = state => state.globalReducer.localIpAddress;
export const getCloudConnection = state => state.globalReducer.cloudConnection;
export const getCurrentNetworkConfig = state =>
  state.globalReducer.currentNetworkConfig;

export const getRestart = state => state.globalReducer.restart;

export const getReset = state => state.globalReducer.reset;

export const getConfigureDynamicIp = state =>
  state.globalReducer.configureDynamicIp;
export const getConfigureStaticIp = state =>
  state.globalReducer.configureStaticIp;
export const getConfigureDynamicWifiIp = state =>
  state.globalReducer.configureDynamicWifiIp;
export const getConfigureStaticWifiIp = state =>
  state.globalReducer.configureStaticWifiIp;
export const getConfigureLteModem = state =>
  state.globalReducer.configureLteModem;
export const getCurrentNetworkConfigList = state =>
  state.globalReducer.currentNetworkConfigList;

export const getSensorStatusList = state =>
  state.globalReducer.sensorStatusList;

import Immutable from "seamless-immutable";
import { types } from "../actions/restart.action";
import { STATUS } from "../../constants";

const INITIAL_STATE = Immutable({
  status: {
    restartStatus: undefined
  },
  errorMessage: undefined,
  restart: undefined
});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.RESTART_REQUESTED:
      window.alert(
        "Please Wait 5 Minutes for Rebooting and Whole System is Stopped"
      );
      return state.merge({
        status: { ...state.status, restartStatus: STATUS.LOADING }
      });
    case types.RESTART_SUCCEEDED:
      return state.merge({
        status: { ...state.status, restartStatus: STATUS.OK },
        restart: action.data
      });
    case types.RESTART_FAILED:
      this.props.history.push("./");
      return state.merge({
        status: { ...state.status, restartStatus: STATUS.ERR },
        errorMessage: action.error
      });
    default:
      return state;
  }
};

export const getRestartStatus = state =>
  state.restartReducer.status.restartStatus;
export const getRestartError = state => state.restartReducer.errorMessage;

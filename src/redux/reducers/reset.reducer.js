import Immutable from "seamless-immutable";
import { types } from "../actions/restart.action";
import { STATUS } from "../../constants";

const INITIAL_STATE = Immutable({
  status: {
    resetStatus: undefined
  },
  errorMessage: undefined,
  reset: undefined
});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.RESET_REQUESTED:
      window.alert("Please Wait a Minute for Resetting");
      return state.merge({
        status: { ...state.status, resetStatus: STATUS.LOADING }
      });
    case types.RESET_SUCCEEDED:
      return state.merge({
        status: { ...state.status, resetStatus: STATUS.OK },
        reset: action.data
      });
    case types.RESET_FAILED:
      this.props.history.push("./");
      return state.merge({
        status: { ...state.status, resetStatus: STATUS.ERR },
        errorMessage: action.error
      });
    default:
      return state;
  }
};

export const getResetStatus = state => state.resetReducer.status.resetStatus;
export const getResetError = state => state.resetReducer.errorMessage;

import Immutable from "seamless-immutable";
import { types } from "../actions/status.action";
import { STATUS } from "../../constants";

const INITIAL_STATE = Immutable({
  status: {
    sensorStatus: undefined,
    sapCountStatus: undefined,
    freeDiskSizeStatus: undefined,
    localIpAddressStatus: undefined,
    cloudConnectionStatus: undefined,
    restartStatus: undefined,
    currentNetworkConfigStatus: undefined,
  },
  errorMessage: undefined,
  sensorStatus: undefined,
  sapCount: undefined,
  freeDiskSize: {
    availableDiskSize: undefined,
    diskSize: undefined
  },
  cloudConnection: undefined,
  localIpAddress: undefined,
  restart: undefined,
  currentNetworkConfig: undefined,

});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.SENSOR_STATUS_API_REQUESTED:
      return state.merge({
        status: { ...state.status, sensorStatus: STATUS.LOADING }
      });
    case types.SENSOR_STATUS_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, sensorStatus: STATUS.OK },
        sensorStatus: action.data
      });
    case types.SENSOR_STATUS_API_FAILED:
      return state.merge({
        status: { ...state.status, sensorStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.SAP_COUNT_API_REQUESTED:
      return state.merge({
        status: { ...state.status, sapCountStatus: STATUS.LOADING }
      });
    case types.SAP_COUNT_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, sapCountStatus: STATUS.OK },
        sapCount: action.data
      });
    case types.SAP_COUNT_API_FAILED:
      return state.merge({
        status: { ...state.status, sapCountStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.FREE_DISK_SIZE_REQUESTED:
      return state.merge({
        status: { ...state.status, freeDiskSizeStatus: STATUS.LOADING }
      });
    case types.FREE_DISK_SIZE_SUCCEEDED:
      return state.merge({
        status: { ...state.status, freeDiskSizeStatus: STATUS.OK },
        freeDiskSize: action.data
      });
    case types.FREE_DISK_SIZE_FAILED:
      return state.merge({
        status: { ...state.status, freeDiskSizeStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.LOCAL_IP_ADDRESS_REQUESTED:
      return state.merge({
        status: { ...state.status, localIpAddressStatus: STATUS.LOADING }
      });
    case types.LOCAL_IP_ADDRESS_SUCCEEDED:
      return state.merge({
        status: { ...state.status, localIpAddressStatus: STATUS.OK },
        localIpAddress: action.data
      });
    case types.LOCAL_IP_ADDRESS_FAILED:
      return state.merge({
        status: { ...state.status, localIpAddressStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.CLOUD_CONNECTION_REQUESTED:
      return state.merge({
        status: { ...state.status, cloudConnectionStatus: STATUS.LOADING }
      });
    case types.CLOUD_CONNECTION_SUCCEEDED:
      return state.merge({
        status: { ...state.status, cloudConnectionStatus: STATUS.OK },
        cloudConnection: action.data
      });
    case types.CLOUD_CONNECTION_FAILED:
      return state.merge({
        status: { ...state.status, cloudConnectionStatus: STATUS.ERR },
        errorMessage: action.error
      });
    case types.CURRENT_NETWORK_CONFIG_API_REQUESTED:
      return state.merge({
        status: { ...state.status, currentNetworkConfigStatus: STATUS.LOADING }
      });
    case types.CURRENT_NETWORK_CONFIG_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, currentNetworkConfigStatus: STATUS.OK },
        currentNetworkConfig: action.data
      });
    case types.CURRENT_NETWORK_CONFIG_API_FAILED:
      return state.merge({
        status: { ...state.status, currentNetworkConfigStatus: STATUS.ERR },
        errorMessage: action.error
      });
    default:
      return state;
  }
};

export const getSensorStatusStatus = state =>
  state.statusReducer.status.sensorStatus;
export const getSensorStatusError = state => state.statusReducer.errorMessage;

export const getSapCountStatus = state =>
  state.statusReducer.status.sapCountStatus;
export const getSapCountError = state => state.statusReducer.errorMessage;

export const getFreeDiskSizeStatus = state =>
  state.statusReducer.status.freeDiskSizeStatus;
export const getFreeDiskSizeError = state => state.statusReducer.errorMessage;

export const getLocalIpAddressStatus = state =>
  state.statusReducer.status.localIpAddressStatus;
export const getLocalIpAddressError = state => state.statusReducer.errorMessage;

export const getCloudConnectionStatus = state =>
  state.statusReducer.status.cloudConnectionStatus;
export const getCloudConnectionError = state => state.statusReducer.errorMessage;

export const getCurrentNetworkConfigStatus = state =>
  state.statusReducer.status.currentNetworkConfigStatus;
export const getCurrentNetworkConfigError = state => state.statusReducer.errorMessage;




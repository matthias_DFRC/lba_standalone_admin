import Immutable from "seamless-immutable";
import { types } from "../actions/sensorsDetails.action";
import { STATUS } from "../../constants";

const INITIAL_STATE = Immutable({
  status: {
    sensorStatusListStatus: undefined
  },
  errorMessage: undefined,
  sensorStatusList: undefined
});

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case types.SENSOR_STATUS_LIST_API_REQUESTED:
      return state.merge({
        status: { ...state.status, sensorStatusListStatus: STATUS.LOADING }
      });
    case types.SENSOR_STATUS_LIST_API_SUCCEEDED:
      return state.merge({
        status: { ...state.status, sensorStatusListStatus: STATUS.OK },
        sensorStatusList: action.data
      });
    case types.SENSOR_STATUS_LIST_API_FAILED:
      return state.merge({
        status: { ...state.status, sensorStatusListStatus: STATUS.ERR },
        errorMessage: action.error
      });
    default:
      return state;
  }
};

export const getSensorStatusListStatus = state =>
  state.sensorsDetailsReducer.status.sensorStatusListStatus;
export const getSensorStatusListError = state =>
  state.sensorsDetailsReducer.errorMessage;

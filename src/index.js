import React from "react";
import ReactDOM from "react-dom";
import "./styles/main.scss";
import "./i18n";
import * as serviceWorker from "./serviceWorker";
import { Provider } from "react-redux";
import configureStore from "./config/store";
import "semantic-ui-css/semantic.min.css";

import Routes from "./config/router";

const store = configureStore();

const app = (
  <Provider store={store}>
    <Routes />
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

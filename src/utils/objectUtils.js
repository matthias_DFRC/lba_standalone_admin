import cloneDeep from 'lodash/cloneDeep';
import isEmpty from 'lodash/isEmpty';
import isEqual from 'lodash/isEqual';
import isObject from 'lodash/isObject';
import transform from 'lodash/transform';

export const deepCloneObject = obj => {
  return cloneDeep(obj);
};

export const deepDiffObject = (object, base) => {
  function changes(object, base) {
    return transform(object, function(result, value, key) {
      if (!isEqual(value, base[key])) {
        result[key] =
          isObject(value) && isObject(base[key])
            ? changes(value, base[key])
            : value;
      }
    });
  }
  return changes(object, base);
};

export const isEmptyObject = obj => {
  return isEmpty(obj);
};

export const objectUtils = {
  deepCloneObject,
  deepDiffObject,
  isEmptyObject
};

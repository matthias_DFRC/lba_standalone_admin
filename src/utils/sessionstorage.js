const get = item => {
  const value = sessionStorage.getItem(item);
  return value ? JSON.parse(value) : null;
};

const set = (item, value) => {
  const strValue = typeof value === "string" ? value : JSON.stringify(value);
  return sessionStorage.setItem(item, strValue);
};

const remove = item => {
  return sessionStorage.removeItem(item);
};

export const sessionstorage = {
  get,
  set,
  remove
};

const isEmpty = str => {
  return !str || (str && !str.replace(/\s/g, '').length);
};

export const stringUtils = {
  isEmpty
};

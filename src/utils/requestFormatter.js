import { MASTER_SERVER_URL } from "../constants";

const loginRequest = ({ username, password }) => ({
  user: username,
  pass: password
});

const siteDetailsRequest = ({ siteId }) => ({
  siteId,
  url: MASTER_SERVER_URL
});

const visitorCountRequest = ({ user, pass, siteId, date, resolution }) => ({
  user,
  pass,
  siteId,
  date,
  resolution
});

const vehicleVisitStatsRequest = ({
  user,
  pass,
  date,
  siteId,
  resolution
}) => ({
  user,
  pass,
  visitDate: date,
  siteId,
  resolution
  // visitDate: date,
  // siteId
});

const simpleRequest = ({ user, pass }) => ({
  user: user,
  pass: pass
});

const simplePostRequest = ({user, pass}) => {
  return {
    user: user,
    pass: pass
  }
}

const configureStaticIpRequest = ({
  user,
  pass,
  ipAddress,
  subnetMask,
  gateway,
  preferredDNS,
  alternateDNS
}) => ({
  user,
  pass,
  ipAddress,
  subnetMask,
  gateway,
  preferredDNS,
  alternateDNS
});

const configureLteModemRequest = ({ user, pass, apn }) => {
  return {
    user,
    pass,
    apn
  }
};

export const formatRequestParams = {
  loginRequest,
  simpleRequest,
  configureStaticIpRequest,
  configureLteModemRequest,
  simplePostRequest
};

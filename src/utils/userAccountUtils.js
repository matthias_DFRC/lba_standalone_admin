import React from "react";
import { SESSION_DATA } from "../constants";
import { stringUtils } from "./stringUtils";

import { Header } from "semantic-ui-react";
import { sessionstorage } from "./sessionstorage";

const getSiteListAsOptions = siteList =>
  siteList.map((site, index) => {
    let text =
      site.name.length > 22 ? `${site.name.slice(0, 20)}...` : site.name;
    return {
      key: index,
      text,
      value: site.id,
      content: (
        <Header as="h4">
          {site.id}
          <Header.Subheader style={{ width: "12em", whiteSpace: "normal" }}>
            {site.name}
          </Header.Subheader>
        </Header>
      )
    };
  });

const isAuthenticated = () => {
  const user_info = sessionstorage.get(SESSION_DATA.authentication);
  return (
    user_info &&
    !(
      stringUtils.isEmpty(user_info["username"]) ||
      stringUtils.isEmpty(user_info["password"])
    )
  );
};

const setLoginSessionInfo = (username, password, sensorId) => {
  sessionstorage.set(SESSION_DATA.authentication, {
    username,
    password,
    sensorId,
  });
  // sessionstorage.set(SESSION_DATA.sites, sites);
};

const setLanguageInfo = lng => {
  sessionstorage.set("language", lng);
};


export const userAccount = {
  getSiteListAsOptions,
  isAuthenticated,
  setLoginSessionInfo,
  setLanguageInfo,
};

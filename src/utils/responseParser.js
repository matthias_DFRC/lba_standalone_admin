import { objectUtils } from './objectUtils';

const availableSitesResp = resp => {
  if (resp && resp.length > 0) {
    let filteredResp = resp.filter(
      response => response.name.includes('휴게소') && response.id > 2570
    );
    return filteredResp.map(site => ({
      id: site.id,
      name: site.name,
      archive: site.archive
    }));
  }

  return resp;
};

const siteDetailsResp = resp => {
  if (!objectUtils.isEmptyObject(resp)) {
    const {
      id,
      name,
      address,
      timezone,
      // mapType,
      maxSiteTracksLifetimeDays,
      siteType,
      populationTypes,
      eventTypes,
      modules,
      // gsm,
      // virtualRegions,
      // regions,
      // blockRegions,
      domains
      // queueSettings
    } = resp;

    const maxSiteTracks = maxSiteTracksLifetimeDays
      ? maxSiteTracksLifetimeDays
      : 4;
    const eventTypesList = eventTypes
      ? eventTypes
          .map(eventType => ({
            id: eventType['eventTypeId'],
            name: eventType['name']
          }))
          .sort((a, b) => (a.id > b.id ? 1 : b.id > a.id ? -1 : 0))
      : [];
    const modulesList = modules.map(module => ({
      id: module.moduleId,
      name: module.name
    }));
    // const regionsList = regionUtils.getRegionsList(regions);
    // const virtualRegionsList = regionUtils.getRegionsList(virtualRegions);
    const domainsList = domains.map(domain => ({
      id: domain.typeDomainId,
      typeDomain: domain.typeDomain,
      domain: domain.domain
    }));

    return {
      siteId: id,
      siteName: name,
      siteAddress: address,
      siteTimezone: timezone,
      // siteMapType: mapType,
      siteType: siteType,
      maxSiteTracksLifetimeDays: maxSiteTracks,
      populationTypesList: populationTypes,
      eventTypesList,
      modulesList,
      // regionsList,
      // virtualRegionsList,
      domainsList
    };
  }

  return resp;
};

const visitorCountResp = resp => {
  if (resp) {
    const visitorCountKey = Object.keys(resp)[4];
    const hourlyVisitorCount = Object.entries(
      resp[visitorCountKey]
    ).map(entry => entry[1]);
    let visitorCountLabel = Object.entries(
      resp[visitorCountKey]
    ).map(entry => entry[0]);
    const durationTimeCount = Object.entries(resp.durationCount)
    .map(entry => entry[1])
    .slice(1, 13);
    let durationTimeLabel = Object.entries(
      resp.durationCount
    ).map(entry => entry[0])
     .slice(1, 13);
    
     durationTimeLabel = durationTimeLabel.map( label => {
       return label.includes('under') === true ? label.replace('under', '<') : label
      }).map( label => {
        return label.includes('from') === true ? label.replace('from', '') : label
      }).map( label => {
        return label.includes('To') === true ? label.replace('To', '-') : label
      }).map( label => {
        return label.includes('over') === true ? label.replace('over', '>') : label
      });

      if( visitorCountLabel[0] === "10") {
        visitorCountLabel = visitorCountLabel.sort();
      }

    return {
      ...resp,
      totalVisitorCountByResolution: hourlyVisitorCount,
      totalVisitorCountByResolutionLabel: visitorCountLabel,
      durationTimeCount,
      durationTimeLabel
    }
  }
  return resp;
};

const vehicleVisitStatsResp = resp => {
  return [...resp].map(vehicle => vehicle.count);
}

export const parseApiResponse = {
  availableSitesResp,
  siteDetailsResp,
  visitorCountResp,
  vehicleVisitStatsResp
};

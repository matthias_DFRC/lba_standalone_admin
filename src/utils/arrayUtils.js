const filterArray = (array, filter, exception) => {
  const searchFilterData = filter.toLowerCase();
  return array.filter(item => {
    return Object.keys(item).some(key => {
      if (!exception.includes(key)) {
        return item[key]
          .toString()
          .toLowerCase()
          .includes(searchFilterData);
      }
    });
  });
};

export const arrayUtils = {
  filterArray
};
